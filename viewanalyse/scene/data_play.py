#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 12:51:12 2019

@author: Tung Thai
"""

import OpenGL.GL as gl
import glfw
import imgui
import numpy as np

from viewanalyse.helper import an_iso_diff
from viewanalyse.unpacker import data_box


def _nearest_pow2(v):
    # From http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
    # Credit: Sean Anderson
    v -= 1
    v |= v >> 1
    v |= v >> 2
    v |= v >> 4
    v |= v >> 8
    v |= v >> 16
    return v + 1


class DataPlay(object):
    """ Something like a bridge between the data_box and the actual rendering """

    def __init__(self):
        self.dat_box = data_box.DataBox()

        # === A bit of an oddball "variable", since it's an executable ============================
        self.func_3d_tex = self._create_3d_texture

        # === Texture stuff wrapping around the data_box data =====================================
        self.tex_id = gl.GLuint()
        self.tex_dim = [0, 0, 0]
        self.tex_pad = [1, 1, 1]

        self.tex_sca = [1, 1, 1]

        self.tex_beg = [0, 0, 0]
        self.tex_spa = [1, 1, 1]

        self.tex_smooth = True

        # === Vertex stuff wrapping around the data_box data ======================================
        self._vex_grid = np.zeros([])
        self._vex_init = np.zeros([])

        self._vex_mask = np.array([])
        self._vex_skip = np.array([])

        self._vex_base = np.zeros([], dtype='f4')
        self.vex_data = np.zeros([], dtype='f4')
        self.vex_line = np.zeros([], dtype='f4')
        self.vex_colo = np.zeros([], dtype='f4')

        self.ver_move = [0, 0, 0]
        self._ver_shift = [0, 0, 0]
        self._ver_skip = [1, 1, 1]
        # self.ver_shift = [0, 0, 12]
        # self.ver_skip = [1, 1, 26]
        self.ver_ftf = True
        # self.ver_mode = 0  # 0 frame-to-frame line, 1 from-init line, 2 from-init dot

        self.ver_p_fact = 3.
        self.ver_l_fact = 5.

        self.ver_show = True
        self.ver_dyn_col = False
        self.ver_dots = True  # show movements as dots if True, else as lines
        self.ver_col_beg = [.3, .8, .3]
        self.ver_col_end = [.8, .3, .3]
        self.ver_lin_size = 5
        self.ver_dot_size = 5

        # === Prep for sine distribution of data, array in disguise of a texture, like a ninja ====
        self.sine_id = gl.GLuint()
        self.sine_size = 1024
        self.sine_data = np.array([], dtype=np.float32)

        self.create_equal_sine([-45., 45.], [-45., 45.])

        # === Prep for transfer function, color party! ============================================
        self.tra_id = gl.GLuint()
        self.tra_size = 256
        self.tra_ends = [[0, 0., 0., 0., 0.], [self.tra_size, 1., 1., 1., 1.]]
        self.tra_aida = [[100, 1., 1., 1., 1.]]
        self.tra_data = np.array([], dtype=np.float32)

        # === Prep for other stuffs ===============================================================
        self.hist_dat = np.zeros([], dtype='f4')
        self.hist_upt = False

        self.item_hover = False

        self.hist_refresh()
        self.create_transfer()

    @property
    def length(self):
        return self.dat_box.vol_len

    def re_set(self):
        self.func_3d_tex = self._create_3d_texture
        self.init_3d_tex()

    def link(self, put_data):
        self.dat_box = put_data
        self.re_set()

        if self.dat_box.vec_exist:
            self.vert_init()
            self.vert_set()

    def hist_refresh(self):
        self.hist_dat = np.histogram(
            self.dat_box.vol_dat_now, bins=range(1, 255))[0]

    def draw_prop(self, value_hover):
        self.item_hover = value_hover

        if imgui.collapsing_header("Histogram", None, 0)[0]:
            imgui.plot_histogram("histogram##draw_prop", np.array(self.hist_dat, dtype='f4'),
                                 graph_size=(imgui.get_content_region_available_width(), 50))
            if imgui.button(" Refresh "):
                self.hist_refresh()
            imgui.same_line()
            self.hist_upt = imgui.checkbox(
                "Update while playing?", self.hist_upt)[1]
        else:
            self.hist_upt = False

        if self.dat_box.vec_exist:
            if imgui.collapsing_header("Voxel movements", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
                self.ver_show = imgui.checkbox(
                    "Show Vertices", self.ver_show)[1]
                self.ver_ftf = imgui.checkbox(
                    "Frame-to-frame", self.ver_ftf)[1]

                self.ver_dots = imgui.checkbox(
                    "Show as dots", self.ver_dots)[1]
                self.ver_dyn_col = imgui.checkbox(
                    "Color dyn.", self.ver_dyn_col)[1]

                change, nv = imgui.drag_float(
                    "vcf##color_vert", self.ver_p_fact, .01, .1, 20.)
                self.check_hover()
                if change:
                    self.ver_p_fact = nv

                change, nv = imgui.drag_int3(
                    "shift##shift_vert", *self._ver_shift, 1, 0, 100)
                self.check_hover()
                if change:
                    self._ver_shift = list(nv)
                    self.vert_set()

                change, nv = imgui.drag_int3(
                    "skip##gap_vert", *self._ver_skip, 1, 1, 100)
                self.check_hover()
                if change:
                    self._ver_skip = [x if x >= 1 else 1 for x in nv]
                    self.vert_set()

                change, new_col = imgui.color_edit3(
                    "col min", *self.ver_col_beg)
                self.check_hover()
                if change:
                    self.ver_col_beg = list(new_col)

                change, new_col = imgui.color_edit3(
                    "col max", *self.ver_col_end)
                self.check_hover()
                if change:
                    self.ver_col_end = list(new_col)

                self.ver_dot_size = imgui.drag_float(
                    "p size", self.ver_dot_size, .25, 1., 100.)[1]
                self.check_hover()

                ch_len, self.ver_lin_size = imgui.drag_float(
                    "l size", self.ver_lin_size, .25, 1., 100.)
                self.check_hover()

                if ch_len:
                    self.ver_l_fact = self.ver_lin_size

                # if imgui.button("Apply change"):
                #     self.vert_set()

        if imgui.collapsing_header("Transfer function", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            imgui.image(
                self.tra_id, imgui.get_content_region_available_width(), 30, (0, 0), (1, 1))
            imgui.push_item_width(imgui.get_content_region_available_width())
            imgui.plot_lines(
                "##red", self.tra_data[3::4], scale_min=.0, scale_max=1., stride=16)

        if imgui.collapsing_header("Manipulate values", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:

            end = self.tra_ends
            mid = self.tra_aida

            total = [end[0]] + mid + [end[1]]
            mid_num = len(total)

            imgui.push_item_width(
                imgui.get_content_region_available_width() - 22)
            imgui.label_text("0", "First position at {}".format(end[0][0]))
            change, new_col = imgui.color_edit4(
                "##col 0", end[0][1], end[0][2], end[0][3], end[0][4])
            if change:
                end[0][1], end[0][2], end[0][3], end[0][4] = new_col
                self.create_transfer()

            for idx in range(1, mid_num - 1):
                imgui.separator()
                imgui.push_item_width(
                    imgui.get_content_region_available_width() - 42)
                if imgui.button("+##add{}".format(idx)):
                    self.transfer_add(idx)
                    glfw.post_empty_event()
                imgui.same_line()
                change, new_val = imgui.slider_int(
                    "{}".format(idx), total[idx][0], total[idx - 1][0], total[idx + 1][0])
                if change:
                    total[idx][0] = new_val
                    self.create_transfer()
                imgui.pop_item_width()
                change, new_col = imgui.color_edit4(
                    "##col {}".format(idx),
                    total[idx][1], total[idx][2], total[idx][3], total[idx][4])
                if change:
                    total[idx][1], total[idx][2], total[idx][3], total[idx][4] = new_col
                    self.create_transfer()
                imgui.same_line()
                if imgui.button("x##del{}".format(idx)):
                    self.transfer_del(idx - 1)
                    self.create_transfer()
                    glfw.post_empty_event()

            imgui.separator()
            if imgui.button("+##add{}".format(mid_num - 1)):
                self.transfer_add(-1)
                glfw.post_empty_event()

            imgui.same_line()
            imgui.label_text("{}".format(mid_num - 1),
                             "Last position at {}".format(end[1][0] - 1))
            change, new_col = imgui.color_edit4("##col {}".format(mid_num - 1),
                                                end[1][1], end[1][2], end[1][3], end[1][4])
            if change:
                end[1][1], end[1][2], end[1][3], end[1][4] = new_col
                self.create_transfer()

        return self.item_hover

    def create_equal_sine(self, range_la, range_el, sine=True):
        # So what's this? Well, large array in disguise of texture, since via uniform means trouble
        la_beg, la_end = np.radians(range_la)
        el_beg, el_end = np.radians(range_el)

        self.sine_data = np.tile(np.linspace(
            0., 1., self.sine_size, dtype=np.float32), (2, 1))

        if sine:
            s_delta = (np.sin(la_end) - np.sin(la_beg)) / (self.sine_size - 1)
            list_la = np.arcsin(
                np.sin(la_beg) + np.arange(0, self.sine_size) * s_delta)
            list_la = np.array(list_la - list_la[0], dtype=np.float32)
            list_la /= list_la[-1]
            self.sine_data[0] = self.sine_data[0] - \
                (list_la - self.sine_data[0])

            s_delta = (np.sin(el_end) - np.sin(el_beg)) / (self.sine_size - 1)
            list_el = np.arcsin(
                np.sin(el_beg) + np.arange(0, self.sine_size) * s_delta)
            list_el = np.array(list_el - list_el[0], dtype=np.float32)
            list_el /= list_el[-1]
            self.sine_data[1] = self.sine_data[1] - \
                (list_el - self.sine_data[1])

        self.sine_data = self.sine_data.flatten()

        gl.glDeleteTextures(self.sine_id)
        gl.glGenTextures(1, self.sine_id)

        gl.glActiveTexture(gl.GL_TEXTURE2)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.sine_id)
        gl.glTexParameteri(
            gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_S, gl.GL_MIRRORED_REPEAT)
        gl.glTexParameteri(
            gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_T, gl.GL_MIRRORED_REPEAT)

        # Yeah.. well.. if using LINEAR, which would be better, it becomes worse..
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        # gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        # gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)

        # Two rows, first row is for azimuth, second one for elevation
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA,
                        self.sine_size, 2, 0, gl.GL_RED, gl.GL_FLOAT, self.sine_data)

    def create_transfer(self):
        lst_all = [self.tra_ends[0], *self.tra_aida, self.tra_ends[1]]

        # expect a list of lists [[position, red, green, blue, alpha], ...]
        lst_len = len(lst_all) - 1
        new_col = [[], [], [], []]

        for idx in range(lst_len):
            bp, br, bg, bb, ba = lst_all[idx]
            ep, er, eg, eb, ea = lst_all[idx + 1]

            new_col[0] = [*new_col[0], *np.linspace(br, er, ep - bp)]
            new_col[1] = [*new_col[1], *np.linspace(bg, eg, ep - bp)]
            new_col[2] = [*new_col[2], *np.linspace(bb, eb, ep - bp)]
            new_col[3] = [*new_col[3], *np.linspace(ba, ea, ep - bp)]

        self.tra_data = np.ones(self.tra_size * 4, dtype=np.float32)
        self.tra_data[0::4] = new_col[0]
        self.tra_data[1::4] = new_col[1]
        self.tra_data[2::4] = new_col[2]
        self.tra_data[3::4] = new_col[3]

        gl.glDeleteTextures(self.tra_id)
        gl.glGenTextures(1, self.tra_id)

        gl.glActiveTexture(gl.GL_TEXTURE1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.tra_id)
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)

        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_MIRRORED_REPEAT)
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_MIRRORED_REPEAT)
        gl.glTexParameteri(
            gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_R, gl.GL_MIRRORED_REPEAT)

        # new_map = colormap.cmap_gnuplot(7, 5, 15, self.tran_size)
        # self.trans_data = colormap.cmap_gnuplot(3, 11, 6, self.tran_size)
        # self.trans_data[3::4] = np.linspace(1.0, 0.0, 256)

        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA,
                        self.tra_size, 1, 0, gl.GL_RGBA, gl.GL_FLOAT, self.tra_data)

    def transfer_add(self, index):
        if index <= 0:
            self.tra_aida.append(self.tra_ends[1].copy())
        else:
            self.tra_aida.insert(index, self.tra_aida[index - 1].copy())

    def transfer_del(self, index):
        del self.tra_aida[index]

    def do_filter(self):
        self.dat_box.set_vol_now(
            an_iso_diff.an_iso_diff3(self.dat_box.vol_dat_now))

    def frame_play(self, forward):
        self.frame_set(self.dat_box.play_jump if forward >
                       0 else -self.dat_box.play_jump, True)

    def frame_set(self, new_time, shift=True):
        self.dat_box.set_play(new_time, shift)
        self.func_3d_tex()

        if self.hist_upt:
            self.hist_refresh()

    def frame_refresh(self):
        self.func_3d_tex()

    def vert_init(self):
        # ============ == prepare grid, only changes with new file ===
        vx, vy, vz = self.dat_box.vol_shape
        self._vex_grid = np.array(
            [[x, y, z] for z in range(vz) for y in range(vy) for x in range(vx)], dtype='f4')

    def vert_set(self, keep_center=True):
        # ============ move the vertices correctly ===
        sha_min = np.multiply(self.dat_box.vol_shape, self.dat_box.min_cut)
        sha_max = np.multiply(self.dat_box.vol_shape, self.dat_box.max_cut)

        if keep_center:
            self.ver_move = (sha_max + sha_min - 1) / 2.
        else:
            self.ver_move = (np.array(self.dat_box.vol_shape) - 1) / 2.

        tmp_mask = np.zeros_like(self.dat_box.d_vec[0], dtype=np.int)

        beg_x, beg_y, beg_z = np.array(
            [int(x) for x in sha_min]) + self._ver_shift
        end_x, end_y, end_z = [int(x) for x in sha_max]
        skip_x, skip_y, skip_z = self._ver_skip

        for ix in range(beg_x, end_x, skip_x):
            tmp_mask[0:, 0:, ix:ix + 1] += 1
        for iy in range(beg_y, end_y, skip_y):
            tmp_mask[0:, iy:iy + 1, 0:] += 1
        for iz in range(beg_z, end_z, skip_z):
            tmp_mask[iz:iz + 1, 0:, 0:] += 1

        self._vex_mask = (tmp_mask >= 3)

    def vert_refresh(self):

        v_init = self.dat_box.d_vec[0]
        v_curr = self.dat_box.vol_vec_now

        if self.ver_ftf:
            true_mask = self._vex_mask & np.logical_not(np.isnan(v_curr))
            v_base = np.extract(true_mask, self._vex_grid)
        else:
            true_mask = self._vex_mask & np.logical_not(np.isnan(v_init))
            v_base = self.vex_data

        v_curr = np.extract(true_mask, self._vex_grid +
                            v_curr.reshape((v_curr.size // 3, 3)))

        if not self.dat_box.play_smooth:
            v_base = v_curr

            # just colors, no dynamic stuff at all
        if not self.ver_dyn_col:
            self.vex_colo = np.zeros(self.vex_data.size, dtype='f4')

        if v_base.size == v_curr.size:
            self.vex_line = np.zeros(((v_base.size * 2) // 3, 3), dtype='f4')
            self.vex_line[0::2] = v_base.reshape((v_base.size // 3, 3))
            self.vex_line[1::2] = v_curr.reshape((v_curr.size // 3, 3))

            tmp_mov = self.vex_line[0::2] - self.vex_line[1::2]

            self.vex_line[1 if self.ver_ftf else 0::2] += tmp_mov * \
                self.ver_lin_size

            # just colors, but with dynamic stuff at all
            if self.ver_dyn_col:
                self.vex_colo = np.sqrt(
                    list(map(dis_p, tmp_mov)), dtype='f4') * self.ver_p_fact
                self.vex_colo = np.where(self.vex_colo > 1, 1, self.vex_colo)

        self.vex_data = v_base if self.ver_ftf else v_curr

    def init_3d_tex(self):
        gl.glDeleteTextures(self.tex_id)
        gl.glGenTextures(1, self.tex_id)

        gl.glActiveTexture(gl.GL_TEXTURE0)
        gl.glBindTexture(gl.GL_TEXTURE_3D, self.tex_id)
        gl.glTexParameteri(
            gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_BORDER)
        gl.glTexParameteri(
            gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_BORDER)
        gl.glTexParameteri(
            gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_R, gl.GL_CLAMP_TO_BORDER)
        # gl.glTexParameteri(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_S, gl.GL_MIRRORED_REPEAT)
        # gl.glTexParameteri(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_T, gl.GL_MIRRORED_REPEAT)
        # gl.glTexParameteri(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_R, gl.GL_MIRRORED_REPEAT)

        if self.tex_smooth:
            gl.glTexParameteri(
                gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
            gl.glTexParameteri(
                gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
        else:
            gl.glTexParameteri(
                gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
            gl.glTexParameteri(
                gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)

        vol_x, vol_y, vol_z = self.dat_box.vol_shape

        p_vol_x = _nearest_pow2(vol_x)
        p_vol_y = _nearest_pow2(vol_y)
        p_vol_z = _nearest_pow2(vol_z)

        tex_side_pad = [
            int((p_vol_z - vol_z) / 2), int((p_vol_y - vol_y) / 2), int((p_vol_x - vol_x) / 2)]

        self.tex_pad = (
            (tex_side_pad[0], p_vol_z - vol_z - tex_side_pad[0]),
            (tex_side_pad[1], p_vol_y - vol_y - tex_side_pad[1]),
            (tex_side_pad[2], p_vol_x - vol_x - tex_side_pad[2]),
        )

        self.tex_dim = [p_vol_x, p_vol_y, p_vol_z]

        self.tex_sca = max(p_vol_x, p_vol_y, p_vol_z) / \
            np.array([p_vol_x, p_vol_y, p_vol_z])

        self.tex_beg = [self.tex_pad[2][0] / p_vol_x,
                        self.tex_pad[1][0] / p_vol_y,
                        self.tex_pad[0][0] / p_vol_z]
        self.tex_spa = [vol_x / p_vol_x, vol_y / p_vol_y, vol_z / p_vol_z]

        self.func_3d_tex()

    def _create_3d_texture(self):
        re_shape = np.pad(self.dat_box.vol_dat_now,
                          self.tex_pad, 'constant', constant_values=.1)
        gl.glBindTexture(gl.GL_TEXTURE_3D, self.tex_id)
        gl.glTexImage3D(gl.GL_TEXTURE_3D, 0, gl.GL_RGBA,
                        *self.tex_dim, 0, gl.GL_RED, gl.GL_UNSIGNED_BYTE, re_shape)
        self.func_3d_tex = self._update_3d_texture

    def _update_3d_texture(self):
        re_shape = np.pad(self.dat_box.vol_dat_now,
                          self.tex_pad, 'constant', constant_values=.1)
        gl.glBindTexture(gl.GL_TEXTURE_3D, self.tex_id)
        gl.glTexSubImage3D(gl.GL_TEXTURE_3D, 0, 0, 0, 0,
                           *self.tex_dim, gl.GL_RED, gl.GL_UNSIGNED_BYTE, re_shape)

    def check_hover(self):
        if imgui.is_mouse_down(0):
            if imgui.is_item_hovered():
                self.item_hover = True
        else:
            self.item_hover = False


def dis_p(n):
    return n[0] * n[0] + n[1] * n[1] + n[2] * n[2]
