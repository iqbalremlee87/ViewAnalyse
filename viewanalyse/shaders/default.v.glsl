#version 430 core

uniform int dmode;

uniform mat4 proj;
uniform mat4 view;
uniform vec3 vol_mov;
uniform vec3 vol_sca;

in vec3 in_Position;
in float in_Col_v;

out float o_col_mix;

const float PI = 3.1415926535897932384626433832795;

void main(void)
{
    if (dmode == 0) {
        gl_Position = proj * view * vec4((in_Position - vol_mov), 1.);
    } else {
        gl_Position = proj * view * vec4((in_Position - vol_mov) * vol_sca, 1.);
    }

    o_col_mix = in_Col_v;
}
