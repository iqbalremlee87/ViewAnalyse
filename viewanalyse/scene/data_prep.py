#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 02 17:13:45 2020

@author: Tung Thai
"""

import numpy as np


def mk_frame(vec_min, vec_max, pad=.005):
    bx, by, bz = vec_min
    ex, ey, ez = vec_max

    x0, x1 = bx - pad, ex + pad
    y0, y1 = by - pad, ey + pad
    z0, z1 = bz - pad, ez + pad

    return np.array([
        x0, y0, z1, x1, y0, z1, x0, y1, z1, x1, y1, z1,
        x0, y0, z1, x0, y1, z1, x1, y0, z1, x1, y1, z1,
        x0, y0, z0, x1, y0, z0, x0, y1, z0, x1, y1, z0,
        x0, y0, z0, x0, y1, z0, x1, y0, z0, x1, y1, z0,
        x0, y0, z0, x0, y0, z1, x0, y1, z0, x0, y1, z1,
        x1, y1, z0, x1, y1, z1, x1, y0, z0, x1, y0, z1
    ], dtype='f4')


def mk_frame_v2(vec_min, vec_max, pad=.005):
    bx, by, bz = vec_min
    ex, ey, ez = vec_max

    x0, x1 = bx - pad, ex + pad
    y0, y1 = by - pad, ey + pad
    z0, z1 = bz - pad, ez + pad

    print("frame v2")

    return np.array([
        # bottom layer
        bx, y0, bz, ex, y0, bz,
        bx, y0, bz, bx, y0, ez,
        ex, y0, ez, ex, y0, bz,
        ex, y0, ez, bx, y0, ez,

        # upper layer
        bx, y1, bz, ex, y1, bz,
        bx, y1, bz, bx, y1, ez,
        ex, y1, ez, ex, y1, bz,
        ex, y1, ez, bx, y1, ez,

        # back layer
        bx, by, z0, ex, by, z0,
        bx, by, z0, bx, ey, z0,
        ex, ey, z0, ex, by, z0,
        ex, ey, z0, bx, ey, z0,

        # front layer
        bx, by, z1, ex, by, z1,
        bx, by, z1, bx, ey, z1,
        ex, ey, z1, ex, by, z1,
        ex, ey, z1, bx, ey, z1,

        # left layer
        x0, by, bz, x0, ey, bz,
        x0, by, bz, x0, by, ez,
        x0, ey, ez, x0, ey, bz,
        x0, ey, ez, x0, by, ez,

        # right layer
        x1, by, bz, x1, ey, bz,
        x1, by, bz, x1, by, ez,
        x1, ey, ez, x1, ey, bz,
        x1, ey, ez, x1, by, ez,

    ], dtype='f4')


def mk_rect(vec_min, vec_max):
    bx, by, bz = vec_min
    ex, ey, ez = vec_max

    return np.array([
        ex, ey, bz, bx, ey, bz,
        ex, ey, ez, bx, ey, ez,
        bx, by, ez, bx, ey, bz,
        bx, by, bz, ex, ey, bz,
        ex, by, bz, ex, ey, ez,
        ex, by, ez, bx, by, ez,
        ex, by, bz, bx, by, bz
    ], dtype='f4')


def mk_cube(vec_min, vec_max):
    bx, by, bz = vec_min
    ex, ey, ez = vec_max

    vtx = [[bx, by, bz],
           [bx, by, ez],
           [bx, ey, bz],
           [bx, ey, ez],
           [ex, by, bz],
           [ex, by, ez],
           [ex, ey, bz],
           [ex, ey, ez]]

    # draw the six faces of the bounding box by drawing triangles
    # draw it counter-clockwise
    # front: 1 5 7 3
    # back: 0 2 6 4
    # left：0 1 3 2
    # right: 7 5 4 6
    # up: 2 3 7 6
    # down: 1 0 4 5
    idx = [1, 5, 7, 7, 3, 1,
           0, 2, 6, 6, 4, 0,
           0, 1, 3, 3, 2, 0,
           7, 5, 4, 4, 6, 7,
           2, 3, 7, 7, 6, 2,
           1, 0, 4, 4, 5, 1]

    return np.array([vtx[i] for i in idx], dtype='f4').flatten()


def mk_plane(min_cut, max_cut, mid_cut):
    bx, by, bz = min_cut
    ex, ey, ez = max_cut
    mx, my, mz = mid_cut

    return np.array([
        mx, by, bz, mx, by, ez, mx, ey, ez,
        mx, by, bz, mx, ey, ez, mx, ey, bz,
        bx, my, bz, bx, my, ez, ex, my, ez,
        bx, my, bz, ex, my, ez, ex, my, bz,
        bx, by, mz, bx, ey, mz, ex, ey, mz,
        bx, by, mz, ex, ey, mz, ex, by, mz
    ], dtype='f4')


def mk_circle(radius, segments=180):
    theta = 2. * 3.1415926 / float(segments)
    c = np.cos(theta)
    s = np.sin(theta)

    x = radius  # radius 1
    y = 0

    tmp_lst = [[0., 0., 0.]] * segments

    for ii in range(segments):
        tmp_lst[ii] = [x, y, 0.]
        t = x
        x = c * x - s * y
        y = s * t + c * y

    return np.array(tmp_lst, dtype='f4')
