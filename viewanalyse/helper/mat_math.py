#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 15:23:58 2019

@author: Tung Thai
"""

import math

import numpy as np


def normalize(vector):
    vector_norm = np.sum(vector * vector)
    if vector_norm == 0:
        return vector
    return vector / math.sqrt(vector_norm)


def move_gpu(move):
    """ Returns a translate matrix in all direction, translation for gpu """
    return np.array([[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, 0],
                     [move[0], move[1], move[2], 1]], dtype='f4')


def move_cpu(move):
    """ Returns a translate matrix in all direction, translation for cpu """
    return np.array([[1, 0, 0, move[0]],
                     [0, 1, 0, move[1]],
                     [0, 0, 1, move[2]],
                     [0, 0, 0, 1]], dtype='f4')


def scale_xyz(scale):
    """ Returns a scaled matrix in each direction individually """
    return np.array([[scale[0], 0, 0, 0],
                     [0, scale[1], 0, 0],
                     [0, 0, scale[2], 0],
                     [0, 0, 0, 1]], dtype='f4')


def scale_all(scale):
    """ Returns a scaled matrix in all direction the same """
    return np.array([[scale, 0, 0, 0],
                     [0, scale, 0, 0],
                     [0, 0, scale, 0],
                     [0, 0, 0, 1]], dtype='f4')


def mat_inv(mat):
    a1, a2, a3, _ = mat[0]
    b1, b2, b3, _ = mat[1]
    c1, c2, c3, _ = mat[2]
    d1, d2, d3, _ = mat[3]

    inv_all = 1 / (
        a3 * b2 * c1 - a2 * b3 * c1 - a3 * b1 * c2 + a1 * b3 * c2 + a2 * b1 * c3 - a1 * b2 * c3)

    inv_a = b3 * c2 * d1 - b2 * c3 * d1 - b3 * c1 * \
        d2 + b1 * c3 * d2 + b2 * c1 * d3 - b1 * c2 * d3
    inv_b = a3 * c2 * d1 - a2 * c3 * d1 - a3 * c1 * \
        d2 + a1 * c3 * d2 + a2 * c1 * d3 - a1 * c2 * d3
    inv_c = a3 * b2 * d1 - a2 * b3 * d1 - a3 * b1 * \
        d2 + a1 * b3 * d2 + a2 * b1 * d3 - a1 * b2 * d3

    return np.array([
        [(b3 * c2 - b2 * c3) * inv_all, -(a3 * c2 - a2 * c3) * inv_all,
         (a3 * b2 - a2 * b3) * inv_all, 0],
        [-(b3 * c1 - b1 * c3) * inv_all, (a3 * c1 - a1 * c3) * inv_all,
         -(a3 * b1 - a1 * b3) * inv_all, 0],
        [(b2 * c1 - b1 * c2) * inv_all, -(a2 * c1 - a1 * c2) * inv_all,
         (a2 * b1 - a1 * b2) * inv_all, 0],
        [-inv_a * inv_all, inv_b * inv_all, -inv_c * inv_all, 1]], dtype='f4')


def mat_transpose3(mat):
    """ transpose and return the upper left 3x3 matrix of a 4x4 matrix """
    return np.array([[mat[0][0], mat[1][0], mat[2][0]],
                     [mat[0][1], mat[1][1], mat[2][1]],
                     [mat[0][2], mat[1][2], mat[2][2]]], dtype='f4')


def rotate_ab(ang_sca):
    """ Returns a rotate_xy * scale matrix, prepared in one go """
    sin_a = math.sin(ang_sca[0])
    cos_a = math.cos(ang_sca[0])
    sin_b = math.sin(ang_sca[1])
    cos_b = math.cos(ang_sca[1])
    scale = ang_sca[2]

    return np.array([[scale * cos_a, scale * sin_b * sin_a, scale * sin_a * cos_b, 0],
                     [0, scale * cos_b, - scale * sin_b, 0],
                     [- scale * sin_a, scale * sin_b *
                         cos_a, scale * cos_a * cos_b, 0],
                     [0, 0, 0, 1]], dtype='f4')


def rotate_abc(angles):
    """ Returns a rotate_xyz matrix """
    sa = math.sin(angles[0])
    ca = math.cos(angles[0])
    sb = math.sin(angles[1])
    cb = math.cos(angles[1])
    sc = math.sin(angles[2])
    cc = math.cos(angles[2])

    return np.array([[ca * cb, ca * sb * sc - sa * cc, ca * sb * cc + sa * sc, 0],
                     [sa * cb, sa * sb * sc + ca * cc, sa * sb * cc - ca * sc, 0],
                     [-sb, cb * sc, cb * cc, 0],
                     [0, 0, 0, 1]], dtype='f4')


def mat_mul3(mat, vec):
    return [mat[0][0] * vec[0] + mat[1][0] * vec[0] + mat[2][0] * vec[0],
            mat[0][1] * vec[1] + mat[1][1] * vec[1] + mat[2][1] * vec[1],
            mat[0][2] * vec[2] + mat[1][2] * vec[2] + mat[2][2] * vec[2]]


def mat_mul4(mat, vec):
    return [mat[0][0] * vec[0] + mat[0][1] * vec[1] + mat[0][2] * vec[2] + mat[0][3] * vec[3],
            mat[1][0] * vec[0] + mat[1][1] * vec[1] +
            mat[1][2] * vec[2] + mat[1][3] * vec[3],
            mat[2][0] * vec[0] + mat[2][1] * vec[1] +
            mat[2][2] * vec[2] + mat[2][3] * vec[3],
            mat[3][0] * vec[0] + mat[3][1] * vec[1] + mat[3][2] * vec[2] + mat[3][3] * vec[3]]


def look_at(eye, target, head):
    """ Returns a numpy array which is used for view calculation """
    forward = normalize(eye - target)
    left = normalize(np.cross(head, forward))
    up = normalize(np.cross(forward, left))
    return np.array([[left[0], up[0], forward[0], 0],
                     [left[1], up[1], forward[1], 0],
                     [left[2], up[2], forward[2], 0],
                     [-np.sum(left * eye), -np.sum(up * eye), -np.sum(forward * eye), 1]],
                    dtype='f4')


def set_frustum(le, ri, bo, to, ne, fa):
    return np.array([[2 * ne / (ri - le), 0, 0, 0],
                     [0, 2 * ne / (to - bo), 0, 0],
                     [(ri + le) / (ri - le), (to + bo) /
                      (to - bo), -(fa + ne) / (fa - ne), -1],
                     [0, 0, -(2 * fa * ne) / (fa - ne), 0]], dtype='f4')


def set_perspective(fov_y, aspect, front, back):
    tangent = math.tan(math.radians(fov_y) * .5)
    height = front * tangent
    width = height * aspect

    return set_frustum(-width, width, -height, height, front, back)


def set_orthographic(le, ri, bo, to, ne, fa):
    return np.array([[2 / (ri - le), 0, 0, 0],
                     [0, 2 / (to - bo), 0, 0],
                     [0, 0, -2 / (fa - ne), 0],
                     [-(ri + le) / (ri - le),
                      -(to + bo) / (to - bo),
                      -(fa + ne) / (fa - ne), 0]], dtype='f4')
