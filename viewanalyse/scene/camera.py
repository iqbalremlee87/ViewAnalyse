#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 16:16:58 2020

@author: Tung Thai
"""

import math

import numpy as np

from viewanalyse.helper import mat_math


def normalize(a3):
    inv_len = 1. / math.sqrt(a3[0] * a3[0] + a3[1] * a3[1] + a3[2] * a3[2])
    return np.array([a3[0] * inv_len, a3[1] * inv_len, a3[2] * inv_len])


def cross(a31, a32):
    return np.array([a31[1] * a32[2] - a31[2] * a32[1],
                     a31[2] * a32[0] - a31[0] * a32[2],
                     a31[0] * a32[1] - a31[1] * a32[0]])


class Camera(object):
    def __init__(self, p_target=None, p_up=None, p_rad=2):
        if p_up is None:
            p_up = [0., 1., 0.]
        if p_target is None:
            p_target = [0., 0., 0.]

        self.target = np.array(p_target)
        self.yaw = .0
        self.pitch = .0
        self.radius = p_rad

        self._position = np.array([])
        self._front = np.array([])
        self._up = np.array([])
        self._right = np.array([])
        self._world_up = np.array(p_up)

        self._orbit_sensitivity = .25
        self._move_sensitivity = .0025

        self._update_cam_vectors()

    def process_moving_side(self, x_mov, y_mov):
        self.target = self.target + (
                self._up * y_mov + self._right * x_mov) * self._move_sensitivity
        self._update_cam_vectors()

    def process_moving_deep(self, y_mov):
        self.target = self.target + self._front * y_mov * self._move_sensitivity
        self._update_cam_vectors()

    def process_orbiting(self, x_mov, y_mov, constrain_pitch=True):
        self.yaw += x_mov * self._orbit_sensitivity
        self.pitch += y_mov * self._orbit_sensitivity

        self.yaw -= 360. if self.yaw >= 180. else 0
        self.yaw += 360. if self.yaw <= -180. else 0

        if constrain_pitch:
            self.pitch = 89.9 if self.pitch > 89.9 else self.pitch
            self.pitch = -89.9 if self.pitch < -89.9 else self.pitch
        self._update_cam_vectors()

    def set_orbiting(self, p_angle, constrain_pitch=True):
        self.yaw, self.pitch = p_angle

        self.yaw -= 360. if self.yaw >= 180. else 0
        self.yaw += 360. if self.yaw <= -180. else 0

        if constrain_pitch:
            self.pitch = 89.9 if self.pitch > 89.9 else self.pitch
            self.pitch = -89.9 if self.pitch < -89.9 else self.pitch
        self._update_cam_vectors()

    def set_distance(self, p_dist):
        self.radius = p_dist
        self._update_cam_vectors()

    def set_moving(self, p_shift):
        self.target = p_shift
        self._update_cam_vectors()

    def reset_target(self):
        self.target = np.array([0., 0., 0.])
        self._update_cam_vectors()

    def reset_orbiting(self):
        self.yaw = self.pitch = 0
        self._update_cam_vectors()

    def reset_distance(self):
        self.radius = 2
        self._update_cam_vectors()

    def get_mat(self):
        return mat_math.look_at(self._position, self.target, self._world_up)

    def _update_cam_vectors(self):
        a = math.cos(math.radians(self.pitch)) * math.sin(math.radians(self.yaw)) * self.radius
        b = math.sin(math.radians(self.pitch)) * self.radius
        c = math.cos(math.radians(self.pitch)) * math.cos(math.radians(self.yaw)) * self.radius

        n_pos = np.array([a, b, c])

        self._position = self.target + n_pos

        self._front = -normalize(n_pos)

        self._right = cross(self._front, self._world_up)
        self._right = normalize(self._right)

        self._up = cross(self._right, self._front)
        self._up = normalize(self._up)


class ArcBall(object):
    def __init__(self):
        self.mat_rot_pre = np.identity(4, dtype='f4')
        self.mat_rot_now = np.identity(4, dtype='f4')

        self.pos_beg = [0, 0]
        self.center = [0, 0]
        self.scr_min = 0

    def reset_view(self):
        self.mat_rot_pre = np.identity(4, dtype='f4')
        self.mat_rot_now = np.identity(4, dtype='f4')

    def set_screen_size(self, w, h):
        self.scr_min = min(w, h)
        self.center = [(w - h) * .5 if w >= h else 0., (h - w) * .5 if w < h else 0.]

    def process_mouse_click(self, x, y):
        self.pos_beg = [x, y]

    def process_mouse_motion(self, x, y):
        va = self._get_arc_vec(self.pos_beg[0], self.pos_beg[1])
        vb = self._get_arc_vec(x, y)
        self._to_mat_now(va, vb)

    def process_mouse_release(self):
        self.mat_rot_pre = np.dot(self.mat_rot_pre, self.mat_rot_now)
        self.mat_rot_now = np.identity(4, dtype='f4')

    def manual_rotate(self, p_yaw, p_pitch):
        a = math.sin(math.radians(p_yaw * .5))
        b = math.sin(math.radians(p_pitch * .5))
        c = math.cos(math.radians(p_yaw * .5)) * math.cos(math.radians(p_pitch * .5))

        self._to_mat_now([0, 0, 1], [a, b, c])
        self.process_mouse_release()

    def manual_roll(self, p_roll):
        a = math.cos(math.radians(p_roll * .5))
        b = math.sin(math.radians(p_roll * .5))
        c = 0.

        self._to_mat_now([1, 0, 0], [a, b, c])
        self.process_mouse_release()

    def get_rot_now(self):
        return np.dot(self.mat_rot_pre, self.mat_rot_now)

    def _to_mat_now(self, beg, end):
        quat = [np.dot(end, beg),
                beg[1] * end[2] - beg[2] * end[1],
                beg[2] * end[0] - beg[0] * end[2],
                beg[0] * end[1] - beg[1] * end[0]]

        _l = quat[0] * quat[0] + quat[1] * quat[1] + quat[2] * quat[2] + quat[3] * quat[3]
        _s = 2. / _l if _l > 0. else 0.

        xs, ys, zs = quat[1] * _s, quat[2] * _s, quat[3] * _s
        wx, wy, wz = quat[0] * xs, quat[0] * ys, quat[0] * zs
        xx, xy, xz = quat[1] * xs, quat[1] * ys, quat[1] * zs
        yy, yz, zz = quat[2] * ys, quat[2] * zs, quat[3] * zs

        self.mat_rot_now[0][0] = 1. - (yy + zz)
        self.mat_rot_now[0][1] = xy + wz
        self.mat_rot_now[0][2] = xz - wy

        self.mat_rot_now[1][0] = xy - wz
        self.mat_rot_now[1][1] = 1. - (xx + zz)
        self.mat_rot_now[1][2] = yz + wx

        self.mat_rot_now[2][0] = xz + wy
        self.mat_rot_now[2][1] = yz - wx
        self.mat_rot_now[2][2] = 1. - (xx + yy)

        self.mat_rot_now[0][3] = self.mat_rot_now[1][3] = 0.
        self.mat_rot_now[2][3] = self.mat_rot_now[3][0] = 0.

        self.mat_rot_now[3][1] = self.mat_rot_now[3][2] = 0.
        self.mat_rot_now[3][3] = 1.

    def _get_arc_vec(self, x, y):
        p = [2. * ((x - self.center[0]) / self.scr_min) - 1.,
             1. - 2. * ((y - self.center[1]) / self.scr_min), 0.]

        op_2 = p[0] * p[0] + p[1] * p[1]

        if op_2 <= 1.:
            p[2] = math.sqrt(1. - op_2)
        else:
            p = normalize(p)
        return p
