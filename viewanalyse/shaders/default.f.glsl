#version 430 core

uniform int dmode;

uniform vec3 col_beg;
uniform vec3 col_end;

in float o_col_mix;
out vec4 color;

void main(void)
{
    if (dmode == 1)
    {
        vec2 pos = gl_PointCoord - vec2(.5);

        if (length(pos) > .5)
        discard;
    }

    color = vec4(mix(col_beg, col_end, o_col_mix), 1.);
}
