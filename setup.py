import setuptools

setuptools.setup(
    name='view-analyser',
    version='0.0.1',
    url='https://gitlab.com/b3683/ViewAnalyse/',
    classifiers=[
        "Private :: Do not Upload"
    ],
    python_requires='>=3.6',
    packages=setuptools.find_packages(),
    install_requires=['numpy', 'imgui[glfw]>=1.1.0', 'Pillow'],
    include_package_data=True,
    package_data={'': ['shaders/*.glsl']},
    entry_points={
        "console_scripts": [
            "viewanalyse = viewanalyse.app:run",
        ]
    }
)
