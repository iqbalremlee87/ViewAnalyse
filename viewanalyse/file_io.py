#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 17:40:34 2020

@author: Tung Thai
"""

import configparser
import os


class FileIO(object):
    # A fancy settings storage wrapper. Will save anything needed and restore them at app start.
    def __init__(self):
        self.app_name = 'view_analyse'

        config_folder = os.path.join(os.path.expanduser('~'), '.config', self.app_name)
        os.makedirs(config_folder, exist_ok=True)
        settings_file = 'settings.conf'
        self.conf_file_path = os.path.join(config_folder, settings_file)

        self.config = configparser.ConfigParser()

        # === from here app settings ===
        self.scr_size = [1600, 900]
        self.scr_bgc = [0., .08, .08, 1.]

        # ==== internal variables for main gui ====
        self.ld_mode = 'npy'
        self.ex_mode = 'npy'

    def write_settings(self):
        self.config['User'] = {
            'scr_size': str(self.scr_size)[1:-1],
            'scr_bgc': str(self.scr_bgc)[1:-1],
            'ld_mode': self.ld_mode,
            'ex_mode': self.ex_mode
        }

        with open(self.conf_file_path, 'w') as configfile:
            self.config.write(configfile)

    def read_settings(self):
        if not os.path.exists(self.conf_file_path):
            self.write_settings()

        self.config.read(self.conf_file_path)

        # === check screen size ===
        raw_scr_size = self.config['User'].get('scr_size', '1600, 900')
        self.scr_size = [int(x) for x in raw_scr_size.split(',')]

        self.scr_size[0] = 800 if self.scr_size[0] < 800 else self.scr_size[0]
        self.scr_size[1] = 600 if self.scr_size[1] < 600 else self.scr_size[1]

        # === check screen color ===
        raw_scr_bgc = self.config['User'].get('scr_bgc', '0., .08, .08, 1.')
        self.scr_bgc = [float(x) for x in raw_scr_bgc.split(',')]

        # === check initial mode ===
        raw_ld_mode = self.config['User'].get('ld_mode', 'npy')
        self.ld_mode = 'npy' if raw_ld_mode not in ['cat', 'sin', 'raw', 'npy'] else raw_ld_mode
