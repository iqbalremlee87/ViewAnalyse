#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 13 16:38:50 2020

@author: Tung Thai
"""

import OpenGL.GL as gl
import glfw
import imgui
import numpy as np

from viewanalyse.helper import mat_math
from viewanalyse.scene import shader, camera, data_play, data_prep
from viewanalyse.unpacker import data_box


class ViewPort(object):
    def __init__(self, window, put_file_io):
        self.glob_conf = put_file_io

        self.window = window

        self.p_cube = {
            'alpha': shader.ShaderProgram("vol.v.glsl", "vol_alpha.f.glsl"),
            'e_abs': shader.ShaderProgram("vol.v.glsl", "vol_emission_abs.f.glsl"),
            'max': shader.ShaderProgram("vol.v.glsl", "vol_max.f.glsl"),
            'mid': shader.ShaderProgram("vol.v.glsl", "vol_mid.f.glsl"),
            'l_max': shader.ShaderProgram("vol.v.glsl", "vol_first_loc_max.f.glsl"),
            'iso': shader.ShaderProgram("vol.v.glsl", "vol_iso.f.glsl")
        }
        self.now_prg = 'alpha'

        self.prg_list = [
            ['Alpha Blend',
             'Emission Absorption',
             'Max Intensity',
             'Mean-value',
             'First local Max',
             'Iso Surface'],
            ['alpha', 'e_abs', 'max', 'mid', 'l_max', 'iso']]
        self.prg_idx = 0

        self.p_plan = shader.ShaderProgram("plane.v.glsl", "plane.f.glsl")
        self.p_norm = shader.ShaderProgram("default.v.glsl", "default.f.glsl")
        self.now_mode = 'cube'

        # ========= The vertexes, for position and color, yes =====================================
        self.vbo_grid_ver = np.zeros([])  # vbo 0, vao 0
        self.vbo_grid_col = np.zeros([])  # vbo 1, vao 0

        self.vbo_cube_ver = np.zeros([])  # vbo 2, vao 1

        self.vbo_plan_ver = np.zeros([])  # vbo 3, vao 2
        self.vbo_plan_tex = np.zeros([])  # vbo 4, vao 2

        self.vbo_vert_ver = np.zeros([])  # vbo 5, vao 3
        self.vbo_vert_col = np.zeros([])  # vbo 6, vao 3

        # number for grouping vertex and colors into one box
        self.vao_id = gl.glGenVertexArrays(4)
        # number of vert and color and norm altogether
        self.vbo_id = gl.glGenBuffers(7)

        # ========= The mighty camera of peerless sight, it might be dazzling =====================
        self.loc_cam = camera.Camera()
        self.loc_arc = camera.ArcBall()

        self.cam_grid = [0, 0, 0]

        self.key_hold = {"control": False, "shift": False}
        self.mouse_buttons = {"left": False, "middle": False, "right": False}
        self.mouse_prev = [0, 0]

        self.play_hold = False
        self.play_val = 0
        self.play_collapsed = False
        self.play_size = [1, 1]

        self.mat_w_proj = np.identity(4, dtype='f4')
        self.mat_g_orth = np.identity(4, dtype='f4')

        self.work_cam = np.identity(4, dtype='f4')
        self.work_cam_inv = np.identity(4, dtype='f4')
        # self.work_mod = np.identity(4, dtype='f4')

        self.vec_cam_inv = np.array([], dtype='f4')

        # ========= Several other stuffs, too lazy to explain them ================================
        self.gui_io = imgui.get_io()

        self.bg_color = [0, 0, 0, 1]

        self.thr_min = .05  # min bright
        self.thr_max = .95  # max bright
        self.vo_step = 250.  # step size
        self.thr_iso = .3  # threshold of iso
        self.shadow_on = 0  # for iso, shall shadow be casted?

        self.man_scale = 1.  # basic scaling

        self.lum_rot = [0, 0]
        self.lum_col = [1., 1., .75]
        self.lum_dist = 10
        self.lum_pos = [0, 0, 0]

        self.set_light()

        self.dat_min_cut = [0., 0., 0.]
        self.dat_max_cut = [1., 1., 1.]
        self.dat_mid_cut = [.5, .5, .5]

        self.center_cut = True

        # ========= Cone transformation part, quite powerful, but still not over 9000 =============
        self.use_sine = True
        self.ls_la_ang = [-45., 45.]
        self.ls_el_ang = [-45., 45.]
        self.pos_box = [0, 0, 0]
        self.pos_vol_dat = [0, .5, .5]
        self.pos_vol_ver = [0, 1, 1]

        self.no_cut_cone = [1., 1., 1.]

        # ========= Decorations and camera ========================================================
        self.cam_mode = True
        self.arc_step = 5.
        self.frame_shown = True
        self.frame_type = False
        self.frame_color = [.4, .4, .2]

        # ========= Vertices, or the movement vectors =============================================
        self.ver_limit = [0, 0]

        self.show_view = False

        self.pin_prop = True
        self.pin_view = True
        self.pin_play = True

        self.item_hover = False

        # ========= Finally, the container, which holds the volume ================================
        self.dat_box = data_box.DataBox()
        self.dat_play = data_play.DataPlay()

        self.dat_ready = False

    def init_mouse(self):
        self.mouse_buttons["left"] = False
        self.mouse_buttons["right"] = False
        self.mouse_buttons["middle"] = False

    def make_vert(self):
        dp = self.dat_play
        if not self.dat_box.vec_exist or not dp.ver_show:
            return

        dp.vert_refresh()

        self.vbo_vert_ver = np.concatenate(
            (dp.vex_data.flatten(), dp.vex_line.flatten()))
        self.vbo_vert_col = np.concatenate(
            (dp.vex_colo, np.repeat(dp.vex_colo, 2, axis=0)))

        self.ver_limit[0] = dp.vex_data.size // 3
        self.ver_limit[1] = dp.vex_line.size // 3

        self.p_norm.upt_buffer(self.vbo_id[5], self.vbo_vert_ver)
        self.p_norm.upt_buffer(self.vbo_id[6], self.vbo_vert_col)

    def set_light(self):
        self.lum_rot[0] = self.lum_rot[0] - \
            360. if self.lum_rot[0] >= 360. else self.lum_rot[0]
        self.lum_rot[0] = 360. + \
            self.lum_rot[0] if self.lum_rot[0] < 0. else self.lum_rot[0]

        self.lum_rot[1] = 90. if self.lum_rot[1] >= 90. else self.lum_rot[1]
        self.lum_rot[1] = -90. if self.lum_rot[1] < -90. else self.lum_rot[1]

        l_rot_y = self.lum_rot[0]  # - self.loc_cam.yaw
        l_rot_p = self.lum_rot[1]  # - self.loc_cam.pitch

        self.lum_pos = [
            np.cos(np.radians(l_rot_p)) *
            np.sin(np.radians(l_rot_y)) * self.lum_dist,
            np.sin(np.radians(l_rot_p)) * self.lum_dist,
            np.cos(np.radians(l_rot_p)) *
            np.cos(np.radians(l_rot_y)) * self.lum_dist
        ]

    def set_models(self):
        if not self.dat_ready:
            return

        if self.center_cut:
            self.pos_box = np.add(self.dat_min_cut, self.dat_max_cut) * .5
        else:
            self.pos_box = [.5, .5, .5]

        w_width, w_height = glfw.get_window_size(self.window)
        self.mat_w_proj = mat_math.set_perspective(
            45., w_width / w_height, .1, 100.)

        self.loc_arc.set_screen_size(w_width, w_height)

        # self.work_mod = mat_math.scale_all(self.man_scale)
        self.work_cam = np.dot(
            self.loc_arc.get_rot_now(), self.loc_cam.get_mat())
        self.work_cam = np.dot(mat_math.scale_all(
            self.man_scale), self.work_cam)

        # calculate, which side is the front
        tmp_mul = mat_math.mat_mul4(self.work_cam, [0, 0, 1, 1])
        self.cam_grid[0] = 0 if tmp_mul[1] > 0 else 1
        self.cam_grid[1] = 0 if tmp_mul[2] > 0 else 1
        self.cam_grid[2] = 0 if tmp_mul[0] > 0 else 1

        tmp_inv_cam = mat_math.mat_inv(self.work_cam)
        self.vec_cam_inv = tmp_inv_cam[3][0:2]

        # this should be the Normal Matrix, needed for lightning
        self.work_cam_inv = mat_math.mat_transpose3(tmp_inv_cam)

    def set_program(self, set_buffer=False):
        gl.glUseProgram(self.p_cube[self.now_prg].p_id)
        # ========= Basics textures =========================================
        self.p_cube[self.now_prg].set_int1('volume', 0)
        self.p_cube[self.now_prg].set_int1('transfer', 1)
        self.p_cube[self.now_prg].set_int1('tra_sine', 2)
        self.p_cube[self.now_prg].set_vec3('tex_beg', self.dat_play.tex_beg)
        self.p_cube[self.now_prg].set_vec3('tex_spa', self.dat_play.tex_spa)
        self.p_cube[self.now_prg].set_vec3('tex_sca', self.dat_play.tex_sca)
        gl.glBindVertexArray(self.vao_id[1])
        self.p_cube[self.now_prg].set_buffer3(
            0, self.vbo_id[2], "in_Position", self.vbo_cube_ver, set_buffer)

        gl.glUseProgram(self.p_plan.p_id)
        # ========= Basics textures =========================================
        self.p_plan.set_int1('volume', 0)
        self.p_plan.set_int1('transfer', 1)
        self.p_plan.set_int1('tra_sine', 2)
        self.p_plan.set_vec3('tex_beg', self.dat_play.tex_beg)
        self.p_plan.set_vec3('tex_spa', self.dat_play.tex_spa)
        self.p_plan.set_vec3('tex_sca', self.dat_play.tex_sca)
        gl.glBindVertexArray(self.vao_id[2])
        self.p_plan.set_buffer3(
            0, self.vbo_id[3], "in_Position", self.vbo_plan_ver, set_buffer)
        self.p_plan.set_buffer3(
            1, self.vbo_id[4], "in_TexCoord", self.vbo_plan_ver, set_buffer)

        gl.glUseProgram(self.p_norm.p_id)
        # ========= Cone transformation =====================================
        self.p_norm.set_vec3(
            'vol_sca', [1. / (max(self.dat_play.tex_dim) - 1)] * 3)
        gl.glBindVertexArray(self.vao_id[3])
        self.p_norm.set_buffer3(
            0, self.vbo_id[5], "in_Position", self.vbo_vert_ver, set_buffer)

        gl.glBindVertexArray(0)
        gl.glUseProgram(0)

    def set_limit(self):
        self.dat_box.set_centering()

        dat_begin = [.5, .5, .5] + np.subtract(
            self.dat_play.tex_beg, [.5, .5, .5]) / self.dat_play.tex_sca
        dat_span = self.dat_play.tex_spa / self.dat_play.tex_sca
        self.dat_min_cut = dat_begin + \
            np.multiply(self.dat_box.min_cut, dat_span)
        self.dat_max_cut = dat_begin + \
            np.multiply(self.dat_box.max_cut, dat_span)
        self.dat_mid_cut = dat_begin + \
            np.multiply(self.dat_box.mid_cut, dat_span)

        # ========= update frame size =========
        if self.frame_type:
            self.vbo_grid_ver = data_prep.mk_frame_v2(
                self.dat_min_cut, self.dat_max_cut)
        else:
            self.vbo_grid_ver = data_prep.mk_frame(
                self.dat_min_cut, self.dat_max_cut)
        self.vbo_grid_col = np.tile(
            np.array(self.frame_color, dtype='f4'), len(self.vbo_grid_ver))
        self.p_norm.upt_buffer(self.vbo_id[0], self.vbo_grid_ver)
        # ========= update vector data, if there is any and if they are actually visible =====
        if self.dat_box.vec_exist:
            self.dat_play.vert_set(self.center_cut)
            self.make_vert()
        # ========= update plane view size and texture coordinates =========
        self.vbo_plan_ver = data_prep.mk_plane(
            self.dat_min_cut, self.dat_max_cut, self.dat_mid_cut)
        self.vbo_plan_tex = self.vbo_plan_ver
        self.p_plan.upt_buffer(self.vbo_id[3], self.vbo_plan_ver)
        self.p_plan.upt_buffer(self.vbo_id[4], self.vbo_plan_ver)
        # ========= update volumetric view size =========
        # self.vbo_cube_ver = data_prep.mk_rect(self.dat_min_cut, self.dat_max_cut)
        self.vbo_cube_ver = data_prep.mk_cube(
            self.dat_min_cut, self.dat_max_cut)
        self.p_cube[self.now_prg].upt_buffer(self.vbo_id[2], self.vbo_cube_ver)

        self.set_models()

    def init_box(self, put_box):
        self.dat_box = put_box
        self.dat_play.link(put_box)
        self.dat_ready = True

        self.pos_vol_ver = (np.array(self.dat_box.vol_shape) - 1) / 2
        # self.pos_vol_ver[0] = 1

        # ========= prepare sine_sine ========================================================
        # self.dat_play.create_equal_sine(
            # self.ls_la_ang, self.ls_el_ang, self.use_sine)
        # ========= setup frame, since set up only once, here directly =======================
        gl.glUseProgram(self.p_norm.p_id)
        self.vbo_grid_ver = data_prep.mk_frame(
            self.dat_min_cut, self.dat_max_cut)
        self.vbo_grid_col = np.zeros(self.vbo_grid_ver.size, dtype='f4')
        gl.glBindVertexArray(self.vao_id[0])
        self.p_norm.set_buffer3(
            0, self.vbo_id[0], "in_Position", self.vbo_grid_ver)
        self.p_norm.set_buffer1(
            1, self.vbo_id[1], "in_Col_v", self.vbo_grid_col)
        if self.dat_box.vec_exist:
            self.make_vert()
            gl.glBindVertexArray(self.vao_id[3])
            self.p_norm.set_buffer3(
                0, self.vbo_id[5], "in_Position", self.vbo_vert_ver)
            self.p_norm.set_buffer1(
                1, self.vbo_id[6], "in_Col_v", self.vbo_vert_col)
        gl.glBindVertexArray(0)
        gl.glUseProgram(0)
        # ========= setup cube and plane, since program can changes, separated method ========
        self.vbo_cube_ver = data_prep.mk_rect(
            self.dat_min_cut, self.dat_max_cut)
        self.vbo_plan_ver = data_prep.mk_plane(
            self.dat_min_cut, self.dat_max_cut, self.dat_mid_cut)
        self.vbo_plan_tex = self.vbo_plan_ver

        self.set_program(True)
        self.set_limit()

        self.set_models()

    def draw_scene(self):
        d_play = self.dat_play

        d_play.frame_refresh()
        # =============== program for norm ====================================
        gl.glUseProgram(self.p_norm.p_id)
        self.p_norm.set_int1('dmode', 0)
        # vertex program
        self.p_norm.set_mat4('view', self.work_cam)
        self.p_norm.set_mat4('proj', self.mat_w_proj)
        self.p_norm.set_vec3('vol_mov', self.pos_box)
        self.p_norm.set_vec3('col_beg', self.frame_color)
        # fragment program
        self.p_norm.set_vec3('tex_sca', d_play.tex_sca)
        # =============== show grid ===========================================
        if self.frame_shown:
            gl.glBindVertexArray(self.vao_id[0])
            if self.frame_type:
                gl.glDrawArrays(gl.GL_LINES, 0 + self.cam_grid[0] * 8, 8)
                gl.glDrawArrays(gl.GL_LINES, 16 + self.cam_grid[1] * 8, 8)
                gl.glDrawArrays(gl.GL_LINES, 32 + self.cam_grid[2] * 8, 8)
            else:
                gl.glDrawArrays(gl.GL_LINES, 0, 24)

        # =============== program for volume ==================================
        if self.now_mode == 'cube':
            gl.glUseProgram(self.p_cube[self.now_prg].p_id)
            # vertex program
            self.p_cube[self.now_prg].set_mat4('view', self.work_cam)
            self.p_cube[self.now_prg].set_mat4('proj', self.mat_w_proj)
            self.p_cube[self.now_prg].set_vec3('vol_mov', self.pos_box)
            # fragment program
            self.p_cube[self.now_prg].set_mat3('mat_norm', self.work_cam_inv)
            self.p_cube[self.now_prg].set_vec3('eye_pos', self.vec_cam_inv)
            self.p_cube[self.now_prg].set_vec3(
                'vol_dim', self.dat_box.vol_shape)

            self.p_cube[self.now_prg].set_vec3('min_cut', self.dat_min_cut)
            self.p_cube[self.now_prg].set_vec3('max_cut', self.dat_max_cut)

            self.p_cube[self.now_prg].set_vec3('d_light', self.lum_pos)
            self.p_cube[self.now_prg].set_vec4('scr_col', self.bg_color)
            self.p_cube[self.now_prg].set_flo1('thr_min', self.thr_min)
            self.p_cube[self.now_prg].set_flo1('thr_max', self.thr_max)
            self.p_cube[self.now_prg].set_flo1('vo_step', self.vo_step)
            self.p_cube[self.now_prg].set_flo1('thr_iso', self.thr_iso)
            self.p_cube[self.now_prg].set_int1('shadow_on', self.shadow_on)

            gl.glActiveTexture(gl.GL_TEXTURE0)
            gl.glBindTexture(gl.GL_TEXTURE_3D, d_play.tex_id)
            gl.glActiveTexture(gl.GL_TEXTURE1)
            gl.glBindTexture(gl.GL_TEXTURE_2D, d_play.tra_id)
            gl.glActiveTexture(gl.GL_TEXTURE2)
            gl.glBindTexture(gl.GL_TEXTURE_2D, d_play.sine_id)

            gl.glBindVertexArray(self.vao_id[1])
            # gl.glDrawArrays(gl.GL_TRIANGLE_STRIP, 0, len(self.vbo_cube_ver) // 3)
            gl.glDrawArrays(gl.GL_TRIANGLES, 0, self.vbo_cube_ver.size // 3)
            gl.glBindTexture(gl.GL_TEXTURE_3D, 0)

        # =============== program for plan ====================================
        if self.now_mode == 'plan':
            gl.glUseProgram(self.p_plan.p_id)
            # vertex program
            self.p_plan.set_mat4('view', self.work_cam)
            self.p_plan.set_mat4('proj', self.mat_w_proj)
            self.p_plan.set_vec3('vol_mov', self.pos_box)
            # fragment program
            self.p_plan.set_vec3('min_cut', self.dat_min_cut)
            self.p_plan.set_vec3('max_cut', self.dat_max_cut)

            self.p_plan.set_vec3('scr_col', self.bg_color)
            self.p_plan.set_flo1('thr_min', self.thr_min)

            gl.glActiveTexture(gl.GL_TEXTURE0)
            gl.glBindTexture(gl.GL_TEXTURE_3D, d_play.tex_id)
            gl.glActiveTexture(gl.GL_TEXTURE1)
            gl.glBindTexture(gl.GL_TEXTURE_2D, d_play.tra_id)
            gl.glActiveTexture(gl.GL_TEXTURE2)
            gl.glBindTexture(gl.GL_TEXTURE_2D, d_play.sine_id)

            gl.glBindVertexArray(self.vao_id[2])
            gl.glDrawArrays(gl.GL_TRIANGLES, 0, len(self.vbo_plan_ver) // 3)
            gl.glBindTexture(gl.GL_TEXTURE_3D, 0)

        # =============== show vector =========================================
        if self.dat_box.vec_exist and d_play.ver_show:
            gl.glDisable(gl.GL_DEPTH_TEST)

            gl.glUseProgram(self.p_norm.p_id)
            self.p_norm.set_vec3('vol_mov', d_play.ver_move)
            self.p_norm.set_vec3('col_beg', d_play.ver_col_beg)
            self.p_norm.set_vec3('col_end', d_play.ver_col_end)

            gl.glBindVertexArray(self.vao_id[3])

            self.p_norm.set_int1('dmode', 1)
            gl.glPointSize(d_play.ver_dot_size)
            gl.glDrawArrays(gl.GL_POINTS, 0, self.ver_limit[0])
            gl.glPointSize(1)

            if not d_play.ver_dots:
                self.p_norm.set_int1('dmode', 2)
                gl.glLineWidth(d_play.ver_dot_size / 2)
                gl.glDrawArrays(
                    gl.GL_LINES, self.ver_limit[0], self.ver_limit[1])
                gl.glLineWidth(1)

            gl.glEnable(gl.GL_DEPTH_TEST)

        # =============== cleanup =============================================
        gl.glBindVertexArray(0)
        gl.glUseProgram(0)

    def gui_playback(self, value_hover, put_play):
        d_box = self.dat_box
        cond_play = imgui.ALWAYS if self.pin_play else imgui.ONCE

        imgui.set_next_window_position(
            4, self.gui_io.display_size.y - 4 - self.play_size[1], cond_play, 0, 0)
        imgui.set_next_window_size(520, -1, cond_play)

        # =============== Playback ============================================
        imgui.begin("Playback view##prop", False,
                    imgui.WINDOW_NO_SAVED_SETTINGS)

        imgui.push_item_width(imgui.get_content_region_available_width())
        cha_frame, val_frame = imgui.slider_int(
            "##play_frame", self.dat_box.play_now, 0, self.dat_play.length - 1,
            "{}/{}".format(self.dat_box.play_now + 1, self.dat_play.length))
        if cha_frame:
            self.play_hold = True
            self.play_val = val_frame
        if not cha_frame and self.play_hold:
            self.play_hold = False
            self.dat_play.frame_set(self.play_val, False)

        if imgui.button("|<"):
            self.dat_play.frame_set(0, False)
        imgui.same_line()
        if imgui.button("<"):
            self.dat_play.frame_set(-1, True)
        imgui.same_line()
        if imgui.button(">"):
            self.dat_play.frame_set(1, True)
        imgui.same_line()
        if imgui.button(">|"):
            self.dat_play.frame_set(-1, False)

        imgui.same_line()
        imgui.push_item_width(80)
        self.dat_box.play_jump = imgui.drag_int(
            "jump", self.dat_box.play_jump, .1, 1, 100)[1]
        self.check_hover()
        imgui.same_line()
        imgui.pop_item_width()

        imgui.push_item_width(60)
        self.dat_box.play_wait = imgui.drag_float(
            "wait", self.dat_box.play_wait, .005, .0, 2., "%.3f", 2.)[1]
        self.check_hover()
        imgui.pop_item_width()

        imgui.same_line()
        self.pin_play = imgui.checkbox("Pin window", self.pin_play)[1]

        self.play_collapsed = imgui.core.is_window_collapsed()
        self.play_size = imgui.core.get_window_size()
        imgui.end()

        return value_hover, put_play

    def draw_view_gui(self, value_hover):
        if not self.show_view:
            return value_hover

        self.item_hover = value_hover
        cond_view = imgui.ALWAYS if self.pin_view else imgui.ONCE

        imgui.set_next_window_position(208, 4, cond_view, 0, 0)
        imgui.set_next_window_size(200, -1, cond_view)

        # =============== viewer ==============================================
        imgui.begin("Viewer properties", False, imgui.WINDOW_NO_SAVED_SETTINGS)
        avail_space = imgui.get_content_region_available_width()

        self.pin_view = imgui.checkbox("Pin window", self.pin_view)[1]
        imgui.separator()

        if imgui.radio_button("Camera", self.cam_mode):
            self.cam_mode = True
        imgui.same_line()
        if imgui.radio_button("ArcBall", not self.cam_mode):
            self.cam_mode = False

        if imgui.collapsing_header("ArcBall view", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            pa_width = avail_space / 3 - 2
            placing = [pa_width + 12, 2 * pa_width + 16]

            # === First row
            if imgui.button("CCLock##arc_rot", pa_width, 25):
                self.loc_arc.manual_roll(self.arc_step)
                self.set_models()
            imgui.same_line(placing[0])
            if imgui.button("Up##arc_rot", pa_width, 25):
                self.loc_arc.manual_rotate(0, self.arc_step)
                self.set_models()
            imgui.same_line(placing[1])
            if imgui.button("ClockW##arc_rot", pa_width, 25):
                self.loc_arc.manual_roll(-self.arc_step)
                self.set_models()

            # === Second row
            if imgui.button("Left##arc_rot", pa_width, 25):
                self.loc_arc.manual_rotate(-self.arc_step, 0)
                self.set_models()
            imgui.same_line(placing[1])
            if imgui.button("Right##arc_rot", pa_width, 25):
                self.loc_arc.manual_rotate(self.arc_step, 0)
                self.set_models()

            # === Third row
            imgui.spacing()
            imgui.same_line(placing[0])
            if imgui.button("Down##arc_rot", pa_width, 25):
                self.loc_arc.manual_rotate(0, -self.arc_step)
                self.set_models()

            imgui.separator()
            imgui.push_item_width(avail_space - 120)
            self.arc_step = imgui.drag_float(
                "Rot step##arc_rot", self.arc_step, .25, .25, 45., "%.2f")[1]
            self.check_hover()
            imgui.pop_item_width()

            imgui.same_line(avail_space - 35)
            if imgui.button("Reset##arc_rot"):
                self.loc_arc.reset_view()
                self.set_models()

        if imgui.collapsing_header("Camera view", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            imgui.push_item_width(avail_space)

            imgui.text("Shift x/y/z")
            imgui.same_line(avail_space - 35)
            if imgui.small_button("Reset##view_shift"):
                self.loc_cam.reset_target()
                self.set_models()

            cha, val = imgui.drag_float3(
                "##camera_shift", *self.loc_cam.target, .005)
            self.check_hover()
            if cha:
                self.loc_cam.set_moving(val)
                self.set_models()

            imgui.text("Angle Yaw/Pitch")
            imgui.same_line(avail_space - 35)
            if imgui.small_button("Reset##view_angle"):
                self.loc_cam.reset_orbiting()
                self.set_models()

            cha, val = imgui.drag_float2(
                "##camera_rotate", self.loc_cam.yaw, self.loc_cam.pitch, .005)
            self.check_hover()
            if cha:
                self.loc_cam.set_orbiting(val)
                self.set_models()

            imgui.text("Zoom Manual/Radius")
            imgui.same_line(avail_space - 35)
            if imgui.small_button("Reset##both_zoom"):
                self.man_scale = 1.
                self.loc_cam.reset_distance()
                self.set_models()

            cha, val = imgui.drag_float2("##both_zoom", self.man_scale, self.loc_cam.radius, .005,
                                         0.01, 100)
            self.check_hover()
            if cha:
                self.man_scale = val[0]
                self.loc_cam.set_distance(val[1])
                self.set_models()

            imgui.pop_item_width()

        if imgui.collapsing_header("Accessories", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            self.frame_shown = imgui.checkbox(
                "Show frame / Color", self.frame_shown)[1]
            ch1, self.frame_type = imgui.checkbox(
                "No-invading frames", self.frame_type)

            imgui.push_item_width(imgui.get_content_region_available_width())
            ch2, new_col = imgui.color_edit3("##bc", *self.frame_color)
            self.check_hover()
            if ch1 or ch2:
                self.frame_color = list(new_col)
                self.set_limit()
                self.p_norm.upt_buffer(self.vbo_id[1], self.vbo_grid_col)
            imgui.pop_item_width()

        imgui.end()
        return self.item_hover

    def draw_prop_gui(self, value_hover):
        d_box = self.dat_box
        self.item_hover = value_hover
        cond_prop = imgui.ALWAYS if self.pin_prop else imgui.ONCE

        imgui.set_next_window_position(4, 4, cond_prop, 0, 0)
        imgui.set_next_window_size(200, -1, cond_prop)

        # =============== properties ==========================================
        imgui.begin("Data properties", False, imgui.WINDOW_NO_SAVED_SETTINGS)

        self.pin_prop = imgui.checkbox("Pin window", self.pin_prop)[1]
        imgui.separator()

        self.show_view = imgui.checkbox(
            "Show view properties", self.show_view)[1]

        if imgui.collapsing_header("Current mode", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            for i, em in enumerate([[' Cubic ', 'cube'], [' Plane ', 'plan']]):
                if imgui.radio_button("{}".format(em[0]), self.now_mode == em[1]):
                    self.now_mode = em[1]
                    self.set_limit()
                if i < 1:
                    imgui.same_line()

            ch, nv = imgui.combo("Shader", self.prg_idx, self.prg_list[0])
            if ch:
                self.prg_idx = nv
                self.now_prg = self.prg_list[1][nv]
                self.set_program()

        if imgui.collapsing_header("Texture range", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:

            ch, self.center_cut = imgui.checkbox(
                "Keep cut centered", self.center_cut)
            if ch:
                self.set_limit()

            for i, em in enumerate(['x', 'y', 'z']):
                if imgui.button("re##ran_{}".format(em)):
                    d_box.min_cut[i], d_box.max_cut[i] = 0., 1.
                    self.set_limit()
                imgui.same_line()
                ch, nv = imgui.drag_float2(
                    '{}##ran'.format(em), d_box.min_cut[i], d_box.max_cut[i], .001, 0., 1., "%.3f")
                self.check_hover()
                if ch:
                    d_box.min_cut[i], d_box.max_cut[i] = nv
                    self.set_limit()
            imgui.separator()
            if imgui.button('Reset all ranges'):
                d_box.reset_limit()
                self.set_limit()

        if self.now_mode == 'plan':
            if imgui.collapsing_header("Layer Cut", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
                for i, em in enumerate(['x', 'y', 'z']):
                    ch, nv = imgui.drag_float('{} plane'.format(em), d_box.mid_cut[i], .001,
                                              d_box.min_cut[i], d_box.max_cut[i])
                    self.check_hover()
                    if ch:
                        d_box.mid_cut[i] = nv
                        self.set_limit()

        if self.now_mode == 'cube':
            if imgui.collapsing_header("Texture layer move", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
                for i, em in enumerate(['x', 'y', 'z']):
                    dist = d_box.max_cut[i] - d_box.min_cut[i]
                    ch, nv = imgui.slider_float(
                        '{} area'.format(em), d_box.min_cut[i], 0., 1. - dist)
                    if ch:
                        d_box.min_cut[i] = nv
                        d_box.max_cut[i] = nv + dist
                        self.set_limit()

        if imgui.collapsing_header("Light config", None, 0)[0]:
            # TODO make this more functional (easier to use) and try to implement SOME visual aid
            imgui.drag_float2('angles', *self.lum_rot, .01)
            self.check_hover()
            imgui.drag_float3('pos', *self.lum_pos)

        if d_box.vec_exist:
            pass

        if imgui.collapsing_header("Data filter", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:
            if self.now_prg == 'iso':
                self.thr_iso = imgui.drag_float(
                    "Iso cut", self.thr_iso, .0005, 0, 1, "%.5f")[1]
                self.check_hover()
                self.shadow_on = 1 if imgui.checkbox(
                    "Shadow", self.shadow_on == 1)[1] else 0
            else:
                self.thr_min, self.thr_max = \
                    imgui.drag_float2('thresh.', self.thr_min,
                                      self.thr_max, .001, 0, 1)[1]
                self.check_hover()

            self.vo_step = imgui.drag_float(
                "steps", self.vo_step, .5, 100., 2000.)[1]
            self.check_hover()

            click, state = imgui.checkbox(
                "Smooth texture", self.dat_play.tex_smooth)
            if click:
                self.dat_play.tex_smooth = state
                self.dat_play.re_set()
                self.set_limit()

            imgui.separator()
            if imgui.button('Reset filter'):
                self.thr_min = .05
                self.thr_max = .95
                self.vo_step = 100.
                self.thr_iso = .3
                self.shadow_on = 0

        imgui.end()
        return self.item_hover

    # noinspection PyUnusedLocal
    def key_call(self, window, key, scan_code, action, mods):
        if not self.dat_ready:
            return

        if action is glfw.PRESS:
            # First the mod keys, aka control, shift etc.
            if key == glfw.KEY_LEFT_CONTROL or key == glfw.KEY_RIGHT_CONTROL:
                self.key_hold['control'] = True

            elif key == glfw.KEY_LEFT_SHIFT or key == glfw.KEY_RIGHT_SHIFT:
                self.key_hold['shift'] = True

            elif key == glfw.KEY_UP:
                self.dat_play.frame_set(1)
                self.make_vert()
            elif key == glfw.KEY_DOWN:
                self.dat_play.frame_set(-1)
                self.make_vert()

            # Because the volume is read only, this won't work (low priority)
            # elif key == glfw.KEY_3:
            #     self.dat_play.do_filter()

        elif action is glfw.RELEASE:
            # First the mod keys, aka control, shift etc.
            if key == glfw.KEY_LEFT_CONTROL or key == glfw.KEY_RIGHT_CONTROL:
                self.key_hold['control'] = False

            elif key == glfw.KEY_LEFT_SHIFT or key == glfw.KEY_RIGHT_SHIFT:
                self.key_hold['shift'] = False

    # noinspection PyUnusedLocal
    def mouse_call(self, window, button, action, mods):
        if not self.dat_ready:
            return

        if button is glfw.MOUSE_BUTTON_LEFT and action is glfw.PRESS:
            self.mouse_buttons["left"] = True
            m_x, m_y = glfw.get_cursor_pos(self.window)
            self.loc_arc.process_mouse_click(m_x, m_y)
        else:
            self.mouse_buttons["left"] = False
            self.loc_arc.process_mouse_release()

        if button is glfw.MOUSE_BUTTON_RIGHT and action is glfw.PRESS:
            self.mouse_buttons['right'] = True
        else:
            self.mouse_buttons['right'] = False

    # noinspection PyUnusedLocal
    def cursor_call(self, window, x_pos, y_pos):
        if not self.dat_ready:
            return

        if self.key_hold['control']:
            if self.mouse_buttons['left']:
                self.lum_rot[0] += (x_pos - self.mouse_prev[0]) * .125
                self.lum_rot[1] += (self.mouse_prev[1] - y_pos) * .125
                self.set_light()

            # if self.mouse_buttons["right"]:
            #     self.loc_cam.process_move_deep(self.mouse_prev[1] - y_pos)
            #     self.set_models()
        else:
            if self.mouse_buttons['left']:
                if self.cam_mode:
                    self.loc_cam.process_orbiting(self.mouse_prev[0] - x_pos,
                                                  y_pos - self.mouse_prev[1])
                else:
                    self.loc_arc.process_mouse_motion(x_pos, y_pos)

                self.set_models()
                self.set_light()

            if self.mouse_buttons['right']:
                self.loc_cam.process_moving_side(self.mouse_prev[0] - x_pos,
                                                 y_pos - self.mouse_prev[1])
                self.set_models()
                self.set_light()

        self.mouse_prev = [x_pos, y_pos]

    # noinspection PyUnusedLocal
    def scroll_call(self, window, x_offset, y_offset):
        if y_offset == 1:
            self.man_scale *= 1.125
        else:
            self.man_scale /= 1.125
        self.set_models()

    def check_hover(self):
        if imgui.is_mouse_down(0):
            if imgui.is_item_hovered():
                self.item_hover = True
        else:
            self.item_hover = False
