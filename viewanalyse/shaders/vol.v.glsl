#version 430 core

uniform mat4 proj;
uniform mat4 view;
uniform vec3 vol_mov;

uniform vec3 eye_pos;

in vec3 in_Position;
out vec3 vray_dir;
flat out vec3 trans_eye;

void main(void)
{
    gl_Position = proj * view * vec4(in_Position - vol_mov, 1.);

    trans_eye = eye_pos + vol_mov;
    vray_dir = in_Position - trans_eye;
}
