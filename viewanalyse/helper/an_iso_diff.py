#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 01 12:24:30 2020

@author: ALI_M, https://pastebin.com/sBsPX4Y7, Tung Thai modified
"""

import numpy as np


def an_iso_diff3(pack, niter=1, kappa=50, gamma=0.1, step=(1., 1., 1.), option=1):
    """
    3D Anisotropic diffusion.

    Usage:
        pack_out = anisodiff(pack, niter, kappa, gamma, option)

    :param option: 1 Perona Malik diffusion equation No 1
                   2 Perona Malik diffusion equation No 2
    :param step: tuple, the distance between adjacent pixels in (z,y,x)
    :param gamma: max value of .25 for stability
    :param kappa: conduction coefficient 20-100 ?
    :param niter: number of iterations
    :param pack: input stack

    Returns:
        pack_out    - diffused stack.

    kappa controls conduction as a function of gradient.  If kappa is low
    small intensity gradients are able to block conduction and hence diffusion
    across step edges.  A large value reduces the influence of intensity
    gradients on conduction.

    gamma controls speed of diffusion (you usually want it at a maximum of
    0.25)

    step is used to scale the gradients in case the spacing between adjacent
    pixels differs in the x,y and/or z axes

    Diffusion equation 1 favours high contrast edges over low contrast ones.
    Diffusion equation 2 favours wide regions over smaller ones.

    Reference:
    P. Perona and J. Malik.
    Scale-space and edge detection using ansotropic diffusion.
    IEEE Transactions on Pattern Analysis and Machine Intelligence,
    12(7):629-639, July 1990.

    Original MATLAB code by Peter Kovesi
    School of Computer Science & Software Engineering
    The University of Western Australia
    pk @ csse uwa edu au
    <http://www.csse.uwa.edu.au>

    Translated to Python and optimised by Alistair Muldal
    Department of Pharmacology
    University of Oxford
    <alistair.muldal@pharm.ox.ac.uk>

    June 2000  original version.
    March 2002 corrected diffusion eqn No 2.
    July 2012 translated to Python
    """

    # ...you could always diffuse each color channel independently if you
    # really want
    if pack.ndim == 4:
        print("Only grayscale stacks allowed, converting to 3D matrix")
        pack = pack.mean(3)

    # initialize output array
    pack = pack.astype('float32')
    pack_out = pack.copy()

    # initialize some internal variables
    delta_s = np.zeros_like(pack_out)
    delta_e = delta_s.copy()
    delta_d = delta_s.copy()
    ns = delta_s.copy()
    ew = delta_s.copy()
    ud = delta_s.copy()
    g_s = np.ones_like(pack_out)
    g_e = g_s.copy()
    g_d = g_s.copy()

    for ii in range(niter):

        # calculate the diffs
        delta_d[:-1, :, :] = np.diff(pack_out, axis=0)
        delta_s[:, :-1, :] = np.diff(pack_out, axis=1)
        delta_e[:, :, :-1] = np.diff(pack_out, axis=2)

        # conduction gradients (only need to compute one per dim!)
        if option == 1:
            g_d = np.exp(-(delta_d / kappa) ** 2.) / step[0]
            g_s = np.exp(-(delta_s / kappa) ** 2.) / step[1]
            g_e = np.exp(-(delta_e / kappa) ** 2.) / step[2]
        elif option == 2:
            g_d = 1. / (1. + (delta_d / kappa) ** 2.) / step[0]
            g_s = 1. / (1. + (delta_s / kappa) ** 2.) / step[1]
            g_e = 1. / (1. + (delta_e / kappa) ** 2.) / step[2]

        # update matrices
        d = g_d * delta_d
        e = g_e * delta_e
        s = g_s * delta_s

        # subtract a copy that has been shifted 'Up/North/West' by one
        # pixel. don't as questions. just do it. trust me.
        ud[:] = d
        ns[:] = s
        ew[:] = e
        ud[1:, :, :] -= d[:-1, :, :]
        ns[:, 1:, :] -= s[:, :-1, :]
        ew[:, :, 1:] -= e[:, :, :-1]

        # update the image
        pack_out += gamma * (ud + ns + ew)

    return pack_out
