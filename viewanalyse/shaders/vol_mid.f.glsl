#version 430 core

uniform sampler3D volume;
uniform sampler2D transfer;
uniform sampler2D tra_sine;

uniform vec3 tex_sca;

// Texture padding removal and Cube cutting or slicing
uniform vec3 tex_beg;
uniform vec3 tex_spa;

uniform vec3 min_cut;
uniform vec3 max_cut;

// The.. uh.. rest
uniform vec4 scr_col;
// uniform vec3 d_light;

uniform float thr_min;
uniform float thr_max;
uniform float vo_step;
//uniform float thr_iso;

in vec3 vray_dir;
flat in vec3 trans_eye;

out vec4 color;

// Pseudo-random number gen from
// http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
// with some tweaks for the range of values
float wang_hash(int seed) {
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return float(seed % 2147483647) / float(2147483647);
}

float vol_pos(vec3 pos) {
    return texture(volume, pos * tex_sca + vec3(.5)).r;
}

vec2 intersect_box(vec3 origin, vec3 dir) {
    vec3 box_min = min_cut;
    vec3 box_max = max_cut;

    vec3 inv_dir = 1. / dir;

    vec3 t_min = (box_min - origin) * inv_dir;
    vec3 t_max = (box_max - origin) * inv_dir;

    vec3 t1 = min(t_min, t_max);
    vec3 t2 = max(t_min, t_max);

    float tNear = max(max(t1.x, t1.y), t1.z);
    float tFar = min(min(t2.x, t2.y), t2.z);

    return vec2(tNear, tFar);
}

void main(void) {
    vec3 direction = normalize(vray_dir);

    vec2 t_hit = intersect_box(trans_eye, direction);
    if (t_hit.x > t_hit.y) discard;
    t_hit.x = max(t_hit.x, 0.);

    float offset = wang_hash(int(gl_FragCoord.x + 640.0 * gl_FragCoord.y));
    float stepsize = 1.73 / vo_step;

    vec3 pos = trans_eye + (t_hit.x + offset * stepsize) * direction;

    int alphacount = 0;
    float out_val = 0.;
    pos -= vec3(.5);

    for (float t = t_hit.x; t < t_hit.y; t += stepsize) {
        float val = vol_pos(pos);

        ++alphacount;
        out_val += val;

        // if (out_val >= thr_max) {
        //     out_val = 1.;
        //     break;
        // }
        pos += direction * stepsize;
    }
    out_val /= alphacount;

    // color = texture(transfer, vec2(out_val, .5));
    color = vec4(out_val, out_val, out_val, 1.);

    if (color.r < thr_min) discard;

    color = mix(scr_col, color, color.a);
}
