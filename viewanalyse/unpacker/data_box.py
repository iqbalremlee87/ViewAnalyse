#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 28 14:53:37 2020

    A cleaner approach to handle various type of data

@author: tungthai
"""

import os
from pathlib import Path

import glfw
import numpy as np


class DataBox(object):
    def __init__(self):
        self.path_vol = Path("/home/kitchen/pan/pancake.raw")
        self.path_vec = Path("/home/kitchen/pan/butter.raw")

        self.name_out = "output"

        self.vol_shape = [1, 1, 1]  # raw shape

        # ========= The containers ============================================
        self._o_vol = [np.array([0]).reshape([1, 1, 1])]
        self.d_vec = []

        # ========= The orientation ===========================================
        self.fix_rotation = [0, 0, 0]
        self.fix_shift = [0, 0, 0]
        self.fix_scale = 1

        self.min_cut = [0, 0, 0]
        self.max_cut = [1, 1, 1]
        self.mid_cut = [.5, .5, .5]

        # ========= Playback stuff - yeah right, nothing special ==============
        self.play_now = 0
        self.play_jump = 1
        self.play_wait = .05
        # False, if playback is not smooth (frame skip or rewind)
        self.play_smooth = True

        # ========= The.. rest! Yes, the awesome.. rest! ======================
        self.exp_range = [0, 100]
        self.exp_type = 'npy'

        self.vec_exist = False

    @property
    def vol_dat_now(self):
        return self._o_vol[self.play_now]

    @property
    def vol_vec_now(self):
        return self.d_vec[self.play_now]

    @property
    def vol_len(self):
        return len(self._o_vol)

    def set_vol_now(self, pack):
        self._o_vol[self.play_now] = pack

    def set_play(self, val, shift=True):
        tmp_old_play = self.play_now
        self.play_now = (
            self.play_now + val) % self.vol_len if shift else val % self.vol_len

        self.play_smooth = False if abs(
            self.play_now - tmp_old_play) > 1 else True

    def reset_limit(self):
        self.min_cut = [0, 0, 0]
        self.max_cut = [1, 1, 1]
        self.mid_cut = [.5, .5, .5]

    def out_type(self, set_type):
        if set_type in ['npy', 'raw']:
            self.exp_type = set_type
        else:
            self.exp_type = 'npy'

    def out_save(self, time_range, wait=False):
        # check range
        beg, end = time_range
        vol_len = len(self._o_vol)
        beg = 0 if beg < 0 else beg
        beg = vol_len if beg > vol_len else beg
        end = 0 if end < 0 else end
        end = vol_len if end > vol_len else end
        if end < beg:
            print("- Nothing to save, invalid range")
            return
        self.exp_range = [beg, end]

        self.export_data(False)

    def set_centering(self):
        self.fix_shift = [0, 0, 0]
        self.fix_scale = .75 / max(self.vol_shape)

    def load_vol_numpy(self, filepath):
        self.path_vol = Path(filepath)

        if self.path_vol.suffix == '.npy':
            self._o_vol = np.load(self.path_vol.as_posix(), mmap_mode='r')
            if not np.issubdtype(self._o_vol.dtype, np.integer):
                self._o_vol = (255 * self._o_vol).astype(np.uint8)
        if self.path_vol.suffix == '.npz':
            self._o_vol = np.load(self.path_vol.as_posix(),
                                  mmap_mode='r')['arr_0']

        self.vol_shape = list(self._o_vol[0].shape)[::-1]

        self.set_centering()
        print("# Added numpy vol {}".format(self.path_vol.name))

    def load_vol_binary(self, filepath, d_type, dimension, val_range):
        self.path_vol = Path(filepath)

        self._o_vol = np.fromfile(self.path_vol, dtype=d_type)

        if d_type is np.float64:
            self._o_vol = np.float32(self._o_vol)

        # TODO some more bugfixes, since this range thingy seems to be still faulty
        if d_type is not np.uint8:
            beg, end = val_range
            self._o_vol = (self._o_vol - beg) * (255. / (end - beg))
            self._o_vol = self._o_vol.astype('f4')

        tmp_t = int(len(self._o_vol) / np.prod(dimension))

        self._o_vol = self._o_vol.reshape([tmp_t] + dimension[::-1])
        self.vol_shape = dimension

        self.set_centering()
        print("# Added binary vol {}".format(self.path_vol.name))

    def load_vec_binary(self, filepath, d_type, dimension):
        self.path_vec = Path(filepath)

        self.d_vec = np.fromfile(self.path_vec, dtype=d_type)

        tmp_t = int(len(self.d_vec) / (np.prod(dimension) * 3))
        if tmp_t < len(self._o_vol):
            print(
                "# oh, vector data has less frames than volume data, proceed with padding")
            self.d_vec = np.pad(self.d_vec, (np.prod(dimension) * 3, 0),
                                'constant', constant_values=(np.nan, 0))
            tmp_t += 1

        self.d_vec = self.d_vec.reshape([tmp_t] + dimension[::-1] + [3])

        self.vec_exist = True
        print("# Added binary vec {}, shape {}".format(
            self.path_vec.name, self.d_vec.shape))

    def load_vec_numpy(self, filepath):
        self.path_vec = Path(filepath)
        self.d_vec = np.load(self.path_vec.as_posix(), mmap_mode='r')
        assert self.d_vec.dtype == np.float32

        # TODO: check correct shape
        # self.d_vec = np.permute(self.d_vec, 0, 2, 3, 4, 1)
        if self.d_vec.shape[1] == 3:
            self.d_vec = np.moveaxis(self.d_vec, 1, -1)

        self.vec_exist = True
        print("# Added numpy vec {}, shape {}".format(
            self.path_vec.name, self.d_vec.shape))

    def export_data(self, compress=False):
        beg, end = self.exp_range
        package = self._o_vol[beg:end]

        if self.exp_type == 'npy':
            pack_name = self.name_out
            if compress:
                np.savez_compressed(self.path_vol.parent / pack_name, package)
            else:
                np.save((self.path_vol.parent / pack_name).as_posix(), package)

        elif self.exp_type == 'raw':
            sz, sy, sx = package[0].shape
            name_size = "_x" + str(sx) + "y" + str(sy) + "z" + str(sz)

            pack_name = self.name_out + name_size + ".dat"
            np.array(package).tofile(self.path_vol.parent / pack_name)

        else:
            print('- Wrong type definition, do nothing')
            return

        print("done\n> Written file to '{}'".format(
            self.path_vol.parent / pack_name))
