#version 420 core

uniform sampler3D volume;
uniform sampler2D transfer;
uniform sampler2D tra_sine;

uniform vec3 tex_sca;

// Texture padding removal and Cube cutting or slicing
uniform vec3 tex_beg;
uniform vec3 tex_spa;

uniform vec3 min_cut;
uniform vec3 max_cut;

// The.. uh.. rest
uniform vec4 scr_col;
uniform vec3 d_light;

uniform float thr_min;
uniform float thr_max;
uniform float vo_step;
uniform float thr_iso;

uniform int shadow_on;

in vec3 vray_dir;
flat in vec3 trans_eye;

out vec4 color;

float stepsize = 1.;

// Pseudo-random number gen from
// http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
// with some tweaks for the range of values
float wang_hash(int seed) {
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return float(seed % 2147483647) / float(2147483647);
}

float vol_pos(vec3 pos) {
    return texture(volume, pos * tex_sca + vec3(.5)).r;
}

vec3 normal(vec3 pos, float intensity) {
    float dx, dy, dz;
    dx = texture(volume, pos + vec3(stepsize * tex_sca.x, 0, 0)).r - intensity;
    dy = texture(volume, pos + vec3(0, stepsize * tex_sca.y, 0)).r - intensity;
    dz = texture(volume, pos + vec3(0, 0, stepsize * tex_sca.z)).r - intensity;
    return -normalize(vec3(dx, dy, dz));
}

vec2 intersect_box(vec3 origin, vec3 dir) {
    vec3 box_min = min_cut;
    vec3 box_max = max_cut;

    vec3 inv_dir = 1. / dir;

    vec3 t_min = (box_min - origin) * inv_dir;
    vec3 t_max = (box_max - origin) * inv_dir;

    vec3 t1 = min(t_min, t_max);
    vec3 t2 = max(t_min, t_max);

    float tNear = max(max(t1.x, t1.y), t1.z);
    float tFar = min(min(t2.x, t2.y), t2.z);

    return vec2(tNear, tFar);
}

void main(void) {
    vec3 direction = normalize(vray_dir);

    vec2 t_hit = intersect_box(trans_eye, direction);
    if (t_hit.x > t_hit.y) discard;
    t_hit.x = max(t_hit.x, 0.);

    float offset = wang_hash(int(gl_FragCoord.x + 640.0 * gl_FragCoord.y));
    stepsize = 1.73 / vo_step;

    vec3 pos = trans_eye + (t_hit.x + offset * stepsize) * direction;

    color = vec4(0.);
    pos -= vec3(.5);

    for (float t = t_hit.x; t < t_hit.y; t += stepsize) {
        float val = vol_pos(pos);

        if (val > thr_iso) {
            pos -= direction * stepsize * .5;
            val = vol_pos(pos);
            pos -= direction * stepsize * (val > thr_iso ? .25 : -.25);
            val = vol_pos(pos);
            vec3 light_dir = normalize(d_light - pos);

            // Blinn-Phong shading
            vec3 N = normal(pos * tex_sca + vec3(.5), val);
            vec3 H = normalize(light_dir + direction);

            float Ia = .1;
            float Id = 1. * max(0, dot(N, light_dir));
            float Is = 8. * pow(max(0, dot(N, H)), 600);

            color = texture(transfer, vec2(val, .5));
            color.rgb = (Ia + Id) * color.rgb + Is * vec3(1.);

            if (shadow_on == 0)
            break;

            vec2 t_out = intersect_box(pos * tex_sca + vec3(.5), light_dir);
            if (t_out.x > t_out.y) break;
            t_out.x = max(t_out.x, 0.);

            pos += light_dir * stepsize;

            float shadow = 0.;
            for (float to = t_out.x + stepsize * 2; to < t_out.y; to += stepsize)
            {
                float block = vol_pos(pos);
                vec4 col_add = texture(transfer, vec2(block, .5));

                shadow = (block > thr_iso) ? (1. - shadow) * col_add.a : -stepsize;

                if (shadow >= thr_max) {
                    color.a = 1.;
                    break;
                }
                pos += light_dir * stepsize;
            }
            color.rgb *= (.5 + (1. - shadow) * .5);
            break;
        }
        pos += direction * stepsize;
    }
    if (color.a < thr_min) discard;
    color = mix(scr_col, color, color.a);
    color.a = 1.;
}
