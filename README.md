# View Analyser

A viewer based on python, glfw and opengl. It makes use of imgui for better user experience. Everything is based on shaders and modern OpenGL. A piece of software developed at the Max Planck Institute for Dynamics and Self-Organization in the past for the purpose to visualize arbitrary medical 3D and 4D data.

![Screenshot of current version Jan. 2022](/media/viewanalyse_main_ui.jpeg)

## Installation

You do not need to install this package into the system, you can run it as it is.

The required packages besides python are:
- numpy
- glfw (available in pip)
- imgui (available in pip)
- pyOpenGl

You can `git clone` it so that you can update everything with `git pull`, `cd` into the directory and run `pip3 install .` to install it with your changes. With `pip3 install -e .` it will create a symlink so that your changes are always included without the need to install it.

## Running the program

Run `viewanalyse` or alternatively `python3 -m viewanalyse` in a terminal to start the program.

There are following features:

For viewing, raytracing is the way to do this,
- Position and view angle manipulation.
- Basic alpha, color and stepping values can be individually configured.
- Transfer function, I know you've waited for this. You can create your own individual colormap. But no save function yet.
- Background color can be changed, Screenshots with [P].
- Various Shaders are available. It is possible to add own if needed. (But sorry for having a customized API for this).
- The data itself can be limited by x, y and z axis to 'frame' only the important parts of the data, if needed.
- It is possible to load accompaning vector data usually extracted from tracking algorithms. Both can be viewed simulaniously.
- Viewer might have trouble loading very large one-frame data, since it is optimized to show 4D videos with limited size, which is usually found in ultrasound data.
- You can load the data by either `-i`/`--input` or directly by drag'n'drop into the window.
