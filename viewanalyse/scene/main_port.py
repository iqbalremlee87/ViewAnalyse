#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 16:14:58 2020

@author: Tung Thai
"""

import os
import re
from datetime import datetime
from pathlib import Path

import OpenGL.GL as gl
import glfw
import imgui
import numpy as np
from PIL import Image
from imgui.integrations.glfw import GlfwRenderer

from viewanalyse.scene import view_port
from viewanalyse.unpacker import data_box
from .. import app


class GraphGLFW(object):
    def __init__(self, put_file_io):
        self.glob_conf = put_file_io
        self.glob_conf.read_settings()

        self.scr_size = self.glob_conf.scr_size
        self.scr_col = self.glob_conf.scr_bgc

        self.win_title = "View Analyzer 0.12"

        # ========= internal variables for gui ================================
        self.win_pos = [0, 0]
        self.win_size = [0, 0]

        self.screen_full = False
        self.show_load = True

        self.fps_count = 0
        self.ready_graph = False

        self.views = {'load': True, 'export': False, 'dash': True,
                      'book': False, 'port_draw': False, 'port_gui': False}
        self.hide_gui = False

        # ========= variables for importing ================================
        self.ld_dat_path = ""
        self.ld_dat_name = ""

        self.ld_vec_path = ""
        self.ld_vec_name = ""
        self.ld_vec_load = False

        self.ld_list = {'raw': ['Raw file'], 'npy': ['Numpy file']}
        self.ld_mode = self.glob_conf.ld_mode
        self.ld_comment = ""

        self.ld_cat_list = []
        self.ld_cat_now = -1
        self.ld_cat_detail = False

        self.d_name = ['float64 (8 byte)', 'float32 (4 byte)',
                       'uint8 (unsigned int 1 byte)', 'int8 (signed int 1 byte)']
        self.d_type = [np.float64, np.float32, np.uint8, np.int8]
        self.d_idx = 2

        self.ld_raw_dim = [256, 256, 256]
        self.ld_raw_typ = self.d_type[self.d_idx]
        self.ld_raw_lim = [0., 255.]

        # ========= variables for exporting ================================
        self.ex_name = ""
        self.ex_path = ""

        self.ex_list = {'raw': 'Raw file', 'npy': 'Numpy file'}
        self.ex_mode = self.glob_conf.ex_mode
        self.ex_ax_len = 100
        self.ex_range = [0, 100]

        # ========= more various stuff ================================
        self.play_back = 0

        self.rec_play = False
        self.rec_count = 0
        self.rec_dir = ""
        self.rec_name = "view_analyse"

        self.ld_info_xml = []
        self.ld_info_raw = []
        self.ld_info_idx = 0
        self.ld_info_show = 0

        self.item_hover = False

        # =====================================================================
        if not glfw.init():
            print("# Error at glfw init")
            return

        # not quite the highest, but not below 4.2, for now
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 2)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        # glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)

        # glfw.window_hint(glfw.TRANSPARENT_FRAMEBUFFER, glfw.TRUE)

        self.window = glfw.create_window(
            *self.scr_size, self.win_title, None, None)
        if not self.window:
            glfw.terminate()
            print("- Failed to create window")
            return

        glfw.make_context_current(self.window)
        glfw.swap_interval(0)  # or in other words, vsync or not

        # ==================== Setup fancy OpenGL extra sausages ==============
        # no glew here, that's only needed for used for C++ due to shaders

        gl.glEnable(gl.GL_DEPTH_TEST)  # enable depth-testing
        gl.glDepthFunc(gl.GL_LESS)

        # gl.glEnable(gl.GL_BLEND)  # enable alpha blending
        # gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        # ==================== Setup ImGui ====================================
        imgui.create_context()
        imgui.style_colors_dark()

        self.impl = GlfwRenderer(self.window)
        self.gui_io = imgui.get_io()

        # ==================== Setup Scene and Loader =========================
        self.l_port_view = view_port.ViewPort(self.window, self.glob_conf)
        self.l_port_view.bg_color = self.scr_col

        # ==================== Assign callbacks ===============================
        glfw.set_window_size_callback(self.window, self.resize_call)
        glfw.set_key_callback(self.window, self.key_call)
        glfw.set_mouse_button_callback(self.window, self.mouse_call)
        glfw.set_cursor_pos_callback(self.window, self.cursor_call)
        glfw.set_scroll_callback(self.window, self.scroll_call)
        glfw.set_drop_callback(self.window, self.drop_call)

        self.ready_graph = True

    def setting_load(self):
        # === This is for loading any settings from file ===
        self.glob_conf.read_settings()

        self.scr_size = self.glob_conf.scr_size
        self.scr_col = self.glob_conf.scr_bgc

    def setting_save(self):
        # === This is for saving any settings to file ===
        self.scr_size = glfw.get_window_size(self.window)
        self.glob_conf.scr_size = self.scr_size
        self.glob_conf.scr_bgc = self.scr_col
        self.glob_conf.ld_mode = self.ld_mode

        self.glob_conf.write_settings()

    def run_app(self):
        if not self.ready_graph:
            return

        time_gap = glfw.get_time()
        last_time = glfw.get_time()
        frame_count = 0

        # start graphical routine
        while not glfw.window_should_close(self.window):
            gl.glClearColor(*self.scr_col)
            gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

            # ================= Start ImGui ===================================
            self.impl.process_inputs()
            imgui.new_frame()

            if self.views['port_draw']:
                self.l_port_view.draw_scene()

            if not self.hide_gui:
                if self.views['port_gui']:
                    self.gui_view_port()
                if self.views['dash']:
                    self.panel_dash()

            if self.views['load']:
                self.dialog_load()
            if self.views['book']:
                self.dialog_catalog()
            if self.views['export']:
                self.dialog_export()

            # ================== show all the properties or gui or whatever =============
            # imgui.show_demo_window()

            imgui.end_frame()
            imgui.render()
            self.impl.render(imgui.get_draw_data())

            # ================== End ImGui ====================================

            glfw.swap_buffers(self.window)

            # DON'T put this into a thread. You'll suffer (from weird random buffer error)
            if self.play_back != 0:
                # something updated all the time as fast as possible
                frame_count += 1

                now_time = glfw.get_time()
                if now_time - last_time >= self.l_port_view.dat_box.play_wait:
                    self.l_port_view.dat_play.frame_play(self.play_back)
                    self.l_port_view.make_vert()

                    last_time = now_time

                if now_time - time_gap >= 1.:
                    self.fps_count = frame_count
                    frame_count = 0
                    time_gap = glfw.get_time()

                if self.rec_play:
                    self.record_tap()

                glfw.post_empty_event()
            glfw.wait_events()

        # === After loop, save the settings automatically
        self.setting_save()

        # === Then initializing the shutdown, cleanup
        self.impl.shutdown()

        glfw.destroy_window(self.window)
        glfw.terminate()

    def gui_view_port(self):
        self.item_hover = self.l_port_view.draw_prop_gui(self.item_hover)
        self.item_hover = self.l_port_view.draw_view_gui(self.item_hover)
        self.item_hover, self.play_back = self.l_port_view.gui_playback(
            self.item_hover, self.play_back)

        if self.item_hover:
            glfw.set_input_mode(self.window, glfw.CURSOR, glfw.CURSOR_DISABLED)
        else:
            glfw.set_input_mode(self.window, glfw.CURSOR, glfw.CURSOR_NORMAL)

    def prep_load(self):
        tmp_box = data_box.DataBox()

        dat_name = (Path(self.ld_dat_path) / self.ld_dat_name).as_posix()
        vec_name = (Path(self.ld_vec_path) / self.ld_vec_name).as_posix()

        if self.ld_mode == 'raw':
            tmp_box.load_vol_binary(
                dat_name, self.ld_raw_typ, self.ld_raw_dim, self.ld_raw_lim)
            if self.ld_vec_load:
                # I assume that the vector data is always in float32 for now
                tmp_box.load_vec_binary(vec_name, 'f4', self.ld_raw_dim)

        if self.ld_mode == 'npy':
            tmp_box.load_vol_numpy(dat_name)
            if self.ld_vec_load:
                tmp_box.load_vec_numpy(vec_name)

        self.l_port_view.init_box(tmp_box)

        self.ld_vec_load = False

        self.views['port_draw'] = True
        self.views['port_gui'] = True

        self.ex_path = self.ld_dat_path
        self.ex_name = self.ld_dat_name.split('.')[0] + '_export'

        self.ld_dat_path = ""
        self.ld_dat_name = ""
        self.views['load'] = False

    def dialog_export(self):
        io_ds = self.gui_io.display_size
        imgui.set_next_window_position(
            io_ds.x * .5, io_ds.y * .5, imgui.ONCE, .5, .5)
        imgui.set_next_window_size(500, -1, imgui.ALWAYS)

        imgui.begin("Export##dialog", False, imgui.WINDOW_NO_SAVED_SETTINGS)

        imgui.text_colored("Export type", 1., 1., .8)
        imgui.begin_child("region_export##type", -1, 35, True)
        for key in self.ex_list:
            if imgui.radio_button(" {}    ".format(self.ex_list[key]), key == self.ex_mode):
                self.ex_mode = key
            imgui.same_line()
        imgui.end_child()

        imgui.spacing()
        imgui.text_colored("Additional options for export", 1., 1., .8)
        imgui.begin_child("region_export##additional", -1, 58, True)

        change, nv = imgui.drag_int2("Data range", *self.ex_range, 1, 0, 9999)
        if change:
            self.ex_range = list(nv)

        imgui.indent()
        l_dat = self.l_port_view.dat_box

        imgui.end_child()

        imgui.spacing()
        imgui.text_colored("File name and path for export", 1., 1., .8)
        imgui.begin_child("region_export##name", -1, 58, True)

        imgui.push_item_width(imgui.get_content_region_available_width() - 35)
        self.ex_path = imgui.input_text(
            "Path##dat_path", self.ex_path, 1024)[1]
        self.ex_name = imgui.input_text("Name##dat_name", self.ex_name, 512)[1]
        imgui.pop_item_width()

        imgui.end_child()

        imgui.spacing()
        imgui.separator()

        if imgui.button(" Export file ", 0, 30):
            l_dat.name_out = self.ex_name
            l_dat.out_type(self.ex_mode)
            l_dat.out_save(self.ex_range, False)

        imgui.same_line()

        imgui.same_line(imgui.get_window_width() - 72)
        if imgui.button(" Cancel ", 0, 30):
            self.views['export'] = False

        imgui.end()

    def dialog_load(self):
        io_ds = self.gui_io.display_size
        imgui.set_next_window_position(
            io_ds.x * .5, io_ds.y * .5, imgui.ONCE, .5, .5)
        imgui.set_next_window_size(600, -1, imgui.ALWAYS)

        imgui.begin("Load - {}###ld_dg".format(self.ld_list[self.ld_mode][0]), False,
                    imgui.WINDOW_NO_SAVED_SETTINGS)
        imgui.text_colored(
            "Choose an import mode and drag and drop file here", 1., .8, .8)
        imgui.dummy(10, 5)
        imgui.separator()

        imgui.text_colored("Import mode", 1., 1., .8)
        imgui.begin_child("region_import", -1, 35, True)
        for key in self.ld_list:
            if imgui.radio_button(" {}    ".format(self.ld_list[key][0]), key == self.ld_mode):
                self.ld_mode = key
                self.ld_comment = ""
            imgui.same_line()
        imgui.end_child()

        if self.ld_mode == 'raw':
            imgui.spacing()

            imgui.text_colored("Additional options for raw import", 1., 1., .8)
            imgui.begin_child("region_add_raw", -1, 81, True)

            self.ld_raw_dim = list(imgui.input_int3(
                'Dimension [x, y, z]', *self.ld_raw_dim)[1])

            change, val = imgui.combo("Data type", self.d_idx, self.d_name)
            if change:
                self.d_idx = val
                self.ld_raw_typ = self.d_type[val]

            self.ld_raw_lim = list(imgui.input_float2(
                'Value range', *self.ld_raw_lim)[1])

            imgui.end_child()

        if self.ld_mode in ['raw', 'npy']:
            imgui.spacing()

            imgui.text_colored(
                "Vector file (for viewing optical flow)", 1., 1., .8)
            imgui.begin_child("region_add_vec", -1,
                              81 if self.ld_vec_load else 35, True)

            ch, vn = imgui.checkbox(
                "Data also has tracking file (float32)", self.ld_vec_load)
            if ch:
                self.ld_vec_load = vn
                glfw.post_empty_event()

            if self.ld_vec_load:
                imgui.push_item_width(
                    imgui.get_content_region_available_width() - 35)
                self.ld_vec_path = imgui.input_text(
                    "Path##vec_path", self.ld_vec_path, 1024)[1]
                self.ld_vec_name = imgui.input_text(
                    "Name##vec_path", self.ld_vec_name, 512)[1]
                imgui.pop_item_width()

            imgui.end_child()

        imgui.spacing()

        imgui.text_colored("File to be loaded", 1., 1., .8)
        imgui.begin_child("region_file", -1, 58, True)

        imgui.push_item_width(imgui.get_content_region_available_width() - 35)
        self.ld_dat_path = imgui.input_text(
            "Path##dat_path", self.ld_dat_path, 1024)[1]
        self.ld_dat_name = imgui.input_text(
            "Name##dat_name", self.ld_dat_name, 512)[1]
        imgui.pop_item_width()

        imgui.end_child()

        imgui.spacing()
        imgui.separator()

        if imgui.button(" Load file ", 0, 30):
            if not self.ld_dat_name:
                self.ld_comment = "No file to load. Sadly."
            else:
                self.ld_comment = "Okay, loading!"
                self.prep_load()

        imgui.same_line()
        imgui.text(" {}".format(self.ld_comment))

        imgui.same_line(imgui.get_window_width() - 72)
        if imgui.button(" Cancel ", 0, 30):
            self.views['load'] = False

        imgui.end()

    def panel_dash(self):
        imgui.set_next_window_position(
            self.gui_io.display_size.x - 4, 4, imgui.ALWAYS, 1, 0)
        imgui.set_next_window_size(270, -1, imgui.ALWAYS)

        flags = imgui.WINDOW_NO_SAVED_SETTINGS | imgui.WINDOW_MENU_BAR

        imgui.begin("Dash##conf", False, flags)

        if imgui.begin_menu_bar():
            if imgui.menu_item('Open file', 'Ctrl+O', False, True)[0]:
                self.views['load'] = True

            if self.l_port_view.dat_ready:
                if imgui.menu_item('Export file', 'Ctrl+E', False, True)[0]:
                    self.views['export'] = True

            imgui.end_menu_bar()

        imgui.text("Background color")
        imgui.push_item_width(imgui.get_content_region_available_width())
        change, new_col = imgui.color_edit4("##bc", *self.scr_col)
        if change:
            self.scr_col = list(new_col)
            self.l_port_view.bg_color = self.scr_col
            gl.glClearColor(*self.scr_col)
        imgui.pop_item_width()

        if self.views['port_gui']:
            l_dat = self.l_port_view.dat_box
            if imgui.collapsing_header("File information", None, imgui.TREE_NODE_DEFAULT_OPEN)[0]:

                info_list = [
                    ('File name', l_dat.path_vol.name),
                    ('Shape', '{}'.format(l_dat.vol_shape)),
                    ('Frame count', '{}'.format(l_dat.vol_len))]

                imgui.columns(2, 'infolist')
                imgui.set_column_offset(1, 90)

                for key, value in info_list:
                    # imgui.separator()
                    imgui.text(key)
                    imgui.next_column()
                    imgui.text_wrapped(value)
                    imgui.next_column()

                imgui.columns(1)
                imgui.spacing()

            imgui.separator()

        self.item_hover = self.l_port_view.dat_play.draw_prop(self.item_hover)

        imgui.end()

    def toggle_full_screen(self):
        if self.screen_full:
            glfw.set_window_monitor(self.window, None, self.win_pos[0], self.win_pos[1],
                                    self.win_size[0], self.win_size[1], glfw.DONT_CARE)
            self.screen_full = False
        else:
            self.win_pos = glfw.get_window_pos(self.window)
            self.win_size = glfw.get_window_size(self.window)

            for elem in glfw.get_monitors():
                m_pos = glfw.get_monitor_pos(elem)
                mode = glfw.get_video_mode(elem)

                if m_pos[0] < self.win_pos[0] <= m_pos[0] + mode.size.width \
                        and m_pos[1] < self.win_pos[1] <= m_pos[1] + mode.size.height:
                    glfw.set_window_monitor(self.window, elem, 0, 0, mode.size.width,
                                            mode.size.height, mode.refresh_rate)
                    self.screen_full = True

    def capture_img(self, put_name):
        gl.glReadBuffer(gl.GL_FRONT)
        pixels = gl.glReadPixels(0, 0, *glfw.get_window_size(self.window), gl.GL_RGBA,
                                 gl.GL_UNSIGNED_BYTE)
        image = Image.frombytes(
            'RGBA', glfw.get_window_size(self.window), pixels)
        image = image.transpose(Image.FLIP_TOP_BOTTOM)

        image_name = "img_{}_{}.png".format(
            put_name, datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
        current_path = os.getcwd()
        image.save(image_name, 'PNG')
        print("# image '{}' saved in path: '{}'".format(image_name, current_path))

    def record_start(self):
        self.rec_play = True
        self.rec_count = 0

        self.rec_dir = "img_series_{}".format(
            datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

        try:
            os.mkdir(self.rec_dir)
        except OSError:
            print("- Creation of directory '{}' failed".format(self.rec_dir))
        else:
            print("# Directory '{}' created for record. Now process..")

    def record_tap(self):
        gl.glReadBuffer(gl.GL_FRONT)
        pixels = gl.glReadPixels(0, 0, *glfw.get_window_size(self.window), gl.GL_RGBA,
                                 gl.GL_UNSIGNED_BYTE)
        image = Image.frombytes(
            'RGBA', glfw.get_window_size(self.window), pixels)
        image = image.transpose(Image.FLIP_TOP_BOTTOM)

        img_save_path = Path(self.rec_dir) / "rec_{}_{:04}.png".format(self.rec_name,
                                                                       self.rec_count)
        image.save(img_save_path, 'PNG')
        self.rec_count += 1

    def record_stop(self):
        self.rec_play = False
        print("# Directory '{}' record of {} frames is done.".format(
            self.rec_dir, self.rec_count))

    # noinspection PyUnusedLocal
    def resize_call(self, window, width, height):
        gl.glViewport(0, 0, width, height)
        self.l_port_view.set_models()

    def key_call(self, window, key, scan_code, action, mods):
        # this is a workaround, which makes sure that interaction with imgui remains after Enter
        self.gui_io.keys_down[256] = self.gui_io.keys_down[257] = 0

        # action regardless of imgui
        if action is glfw.PRESS:
            if mods == glfw.MOD_CONTROL:
                if key == glfw.KEY_Q:  # Ctrl+Q Quit the program
                    glfw.set_window_should_close(self.window, True)

                if key == glfw.KEY_SPACE:
                    self.hide_gui = not self.hide_gui
                    print("# Hide GUI set to {}".format(self.hide_gui))

        if action is glfw.RELEASE:
            if self.views['port_gui']:
                if key == glfw.KEY_LEFT or key == glfw.KEY_RIGHT:
                    self.play_back = 0

        # action with regards of imgui
        if self.gui_io.want_capture_keyboard:
            self.impl.keyboard_callback(window, key, scan_code, action, mods)
        else:
            if action is glfw.PRESS:
                if mods == glfw.MOD_CONTROL:

                    if key == glfw.KEY_SPACE:
                        self.hide_gui = not self.hide_gui
                        print("# Hide GUI set to {}".format(self.hide_gui))
                else:
                    if key == glfw.KEY_SPACE:
                        if self.views['port_gui']:
                            self.play_back = 1 - self.play_back

                    if key == glfw.KEY_P:
                        self.capture_img("viewanalyse")

                    if key == glfw.KEY_F:
                        self.toggle_full_screen()

                    if key == glfw.KEY_R and self.play_back:
                        self.record_start()

                    if self.views['port_gui']:
                        if key == glfw.KEY_LEFT:
                            self.play_back = -1
                        if key == glfw.KEY_RIGHT:
                            self.play_back = 1

            if action is glfw.RELEASE:
                if key == glfw.KEY_R and self.play_back:
                    self.record_stop()

                if self.views['port_gui']:
                    if key == glfw.KEY_LEFT or key == glfw.KEY_RIGHT:
                        self.play_back = 0

            self.l_port_view.key_call(window, key, scan_code, action, mods)

    def mouse_call(self, window, button, action, mods):
        self.l_port_view.init_mouse()

        if self.gui_io.want_capture_mouse:
            self.impl.mouse_callback(window, button, action, mods)
        else:
            self.l_port_view.mouse_call(window, button, action, mods)

        if action is glfw.RELEASE:
            glfw.post_empty_event()

    def cursor_call(self, window, x_pos, y_pos):
        if not self.gui_io.want_capture_mouse:
            self.l_port_view.cursor_call(window, x_pos, y_pos)

    def scroll_call(self, window, x_offset, y_offset):
        if self.gui_io.want_capture_mouse:
            self.impl.scroll_callback(window, x_offset, y_offset)
        else:
            self.l_port_view.scroll_call(window, x_offset, y_offset)

    # noinspection PyUnusedLocal
    def drop_call(self, window, path):
        results = Path(path[0]).resolve()
        if results:
            self.ld_comment = "File path checked."
        else:
            self.ld_comment = "Dropped file or path seems to be invalid."

        if self.ld_vec_load:
            self.ld_vec_path = results.parent.as_posix() if results else ""
            self.ld_vec_name = results.name if results else ""
        else:
            self.ld_dat_path = results.parent.as_posix() if results else ""
            self.ld_dat_name = results.name if results else ""

        self.views['load'] = True
