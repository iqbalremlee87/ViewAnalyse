#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from pathlib import Path

from . import file_io
from .scene import main_port
from .unpacker import data_box


def run():
    parser = argparse.ArgumentParser(
        prefix_chars='-+', description='Viewer for 4 dimensional data')

    parser.add_argument("-i", "--input", help="input path for opening file")

    args = parser.parse_args()

    if args.input is None:
        # if there are no parameters given, just start viewer directly
        l_file_io = file_io.FileIO()

        loc_graph = main_port.GraphGLFW(l_file_io)
        loc_graph.run_app()

    else:
        l_file_io = file_io.FileIO()

        loc_graph = main_port.GraphGLFW(l_file_io)
        loc_graph.drop_call(None, [args.input])

        loc_graph.run_app()


if __name__ == '__main__':
    run()
