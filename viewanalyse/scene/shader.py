#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 11:45:12 2019

@author: Tung Thai
"""

import pkgutil

import OpenGL.GL as gl
from OpenGL.arrays import ArrayDatatype as ArDa


def add_shader(source, shader_type, root="../shaders/"):
    """ Helper function for compiling a GLSL shader
    :param root: str
        Pre-defined root, so full path can be omitted
    :param source: str
        String of filename with shader source code
    :param shader_type: valid OpenGL shader type
        Type of shader to compile
    :return: int
        Identifier for shader if compilation is successful
    """
    shader_prep = pkgutil.get_data(__package__, root + source).decode("utf-8")

    shader_id = gl.glCreateShader(shader_type)
    gl.glShaderSource(shader_id, shader_prep)
    gl.glCompileShader(shader_id)
    if gl.glGetShaderiv(shader_id, gl.GL_COMPILE_STATUS) != gl.GL_TRUE:
        info = gl.glGetShaderInfoLog(shader_id)
        raise RuntimeError("Shader compilation failed: {}".format(info))
    return shader_id


class ShaderProgram(object):
    """ Helper class for using GLSL shader programs """

    def __init__(self, vertex, fragment):
        """ Helper class for using GLSL shader programs
        :param vertex: str
            String containing shader source code for the vertex shader
        :param fragment: str
            String containing shader source code for the fragment header
        """
        vs_id = add_shader(vertex, gl.GL_VERTEX_SHADER)
        frag_id = add_shader(fragment, gl.GL_FRAGMENT_SHADER)

        self.p_id = gl.glCreateProgram()

        gl.glAttachShader(self.p_id, vs_id)
        gl.glAttachShader(self.p_id, frag_id)
        gl.glLinkProgram(self.p_id)

        if gl.glGetProgramiv(self.p_id, gl.GL_LINK_STATUS) != gl.GL_TRUE:
            info = gl.glGetProgramInfoLog(self.p_id)
            gl.glDeleteProgram(self.p_id)
            gl.glDeleteShader(vs_id)
            gl.glDeleteShader(frag_id)
            raise RuntimeError("Program compilation failed: {}".format(info))
        gl.glDeleteShader(vs_id)
        gl.glDeleteShader(frag_id)

    def __del__(self):
        gl.glDeleteProgram(self.p_id)

    def attribute_loc(self, name):
        return gl.glGetAttribLocation(self.p_id, name)

    def bind_uniform_buffer(self, ubo_name, ubo_bind):
        idx = gl.glGetUniformBlockIndex(self.p_id, ubo_name)
        gl.glUniformBlockBinding(self.p_id, idx, ubo_bind)

    def set_int1(self, name, num):
        gl.glUniform1i(gl.glGetUniformLocation(self.p_id, name), num)

    def set_flo1(self, name, num):
        gl.glUniform1f(gl.glGetUniformLocation(self.p_id, name), num)

    def set_vec3(self, name, vec):
        gl.glUniform3fv(gl.glGetUniformLocation(self.p_id, name), 1, vec)

    def set_vec4(self, name, vec):
        gl.glUniform4fv(gl.glGetUniformLocation(self.p_id, name), 1, vec)

    def set_mat3(self, name, mat):
        gl.glUniformMatrix3fv(gl.glGetUniformLocation(self.p_id, name), 1, gl.GL_FALSE, mat)

    def set_mat4(self, name, mat):
        gl.glUniformMatrix4fv(gl.glGetUniformLocation(self.p_id, name), 1, gl.GL_FALSE, mat)

    def set_buffer1(self, vert_attr, n_vbo, name, data, set_buffer=True):
        gl.glEnableVertexAttribArray(vert_attr)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, n_vbo)
        if set_buffer:
            gl.glBufferData(gl.GL_ARRAY_BUFFER, ArDa.arrayByteCount(data), data, gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(self.attribute_loc(name), 1, gl.GL_FLOAT, gl.GL_FALSE, 0, None)

    def set_buffer3(self, vert_attr, n_vbo, name, data, set_buffer=True):
        gl.glEnableVertexAttribArray(vert_attr)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, n_vbo)
        if set_buffer:
            gl.glBufferData(gl.GL_ARRAY_BUFFER, ArDa.arrayByteCount(data), data, gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(self.attribute_loc(name), 3, gl.GL_FLOAT, gl.GL_FALSE, 0, None)

    def set_buffer4(self, vert_attr, n_vbo, name, data, set_buffer=True):
        gl.glEnableVertexAttribArray(vert_attr)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, n_vbo)
        if set_buffer:
            gl.glBufferData(gl.GL_ARRAY_BUFFER, ArDa.arrayByteCount(data), data, gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(self.attribute_loc(name), 4, gl.GL_FLOAT, gl.GL_FALSE, 0, None)

    @staticmethod
    def upt_buffer(n_vbo, data):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, n_vbo)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, ArDa.arrayByteCount(data), data, gl.GL_STATIC_DRAW)
