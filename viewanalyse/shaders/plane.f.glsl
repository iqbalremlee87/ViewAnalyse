#version 430 core
//precision highp int;
//precision highp float;

uniform sampler3D volume;
uniform sampler2D transfer;
uniform sampler2D tra_sine;

uniform vec3 vol_dim;
uniform vec3 tex_sca;
uniform vec3 dat_sca;

// Texture padding removal and Cube cutting or slicing
uniform vec3 tex_beg;
uniform vec3 tex_spa;

uniform vec3 min_cut;
uniform vec3 max_cut;

// The.. uh.. rest
uniform vec3 scr_col;
uniform float thr_min;

in vec3 o_color;
in vec3 o_texco;

out vec4 color;

void main(void)
{
    // Tranformatio part
    float ytan, ztan, ran, pos_ny, pos_nz, val;

    val = texture(volume, (o_texco - vec3(.5)) * tex_sca + vec3(.5)).r;

    if (val < thr_min) discard;

    color = texture(transfer, vec2(val, .5));

    color.rgb = color.a * color.rgb + (1. - color.a) * scr_col;
    color.a = 1.;
}
