#version 430 core

uniform mat4 proj;
uniform mat4 view;
uniform vec3 vol_mov;

in vec3 in_Position;
in vec3 in_TexCoord;

out vec3 o_texco;

void main(void)
{
    gl_Position = proj * view * vec4(in_Position - vol_mov, 1.);
    o_texco = in_TexCoord;
}
