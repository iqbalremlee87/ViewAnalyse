#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 14:28:37 2020

@author: Tung Thai, based on rgbformulae of gnuplot
"""

import numpy as np


def _formula_func(f_idx, array):
    """ This function is rewritten to work for numpy arrays, since it should be faster this way """
    if f_idx < 0:
        array = 1.0 - array
        f_idx = -f_idx

    tmp = np.zeros_like(array)
    if f_idx == 0:
        tmp = np.full_like(array, .0)
    elif f_idx == 1:
        tmp = np.full_like(array, .5)
    elif f_idx == 2:
        tmp = np.full_like(array, 1.)
    elif f_idx == 3:
        tmp = array
    elif f_idx == 4:
        tmp = array * array
    elif f_idx == 5:
        tmp = array * array * array  # don't you dare to replace these with np.power, it' slower
    elif f_idx == 6:
        tmp = array * array * array * array  # if you disagree to this, please provide benchmarks
    elif f_idx == 7:
        tmp = np.sqrt(array)
    elif f_idx == 8:
        tmp = np.sqrt(np.sqrt(array))
    elif f_idx == 9:
        tmp = np.sin(np.pi * .5 * array)
    elif f_idx == 10:
        tmp = np.cos(np.pi * .5 * array)
    elif f_idx == 11:
        tmp = np.abs(array - .5)
    elif f_idx == 12:
        tmp = (2. * array - 1.) * (2. * array - 1.)
    elif f_idx == 13:
        tmp = np.sin(np.pi * array)
    elif f_idx == 14:
        tmp = np.abs(np.cos(np.pi * array))
    elif f_idx == 15:
        tmp = np.sin(np.pi * 2. * array)
    elif f_idx == 16:
        tmp = np.cos(np.pi * 2. * array)
    elif f_idx == 17:
        tmp = np.abs(np.sin(np.pi * 2. * array))
    elif f_idx == 18:
        tmp = np.abs(np.cos(np.pi * 2. * array))
    elif f_idx == 19:
        tmp = np.abs(np.sin(np.pi * 4. * array))
    elif f_idx == 20:
        tmp = np.abs(np.cos(np.pi * 4. * array))
    elif f_idx == 21:
        tmp = 3. * array
    elif f_idx == 22:
        tmp = 3. * array - 1.
    elif f_idx == 23:
        tmp = 3. * array - 2.
    elif f_idx == 24:
        tmp = np.abs(3. * array - 1.)
    elif f_idx == 25:
        tmp = np.abs(3. * array - 2.)
    elif f_idx == 26:
        tmp = 1.5 * array - .5
    elif f_idx == 27:
        tmp = 1.5 * array - 1.
    elif f_idx == 28:
        tmp = np.abs(1.5 * array - .5)
    elif f_idx == 29:
        tmp = np.abs(1.5 * array - 1.)
    elif f_idx == 30:
        tmp = np.where(array <= .25, 0., array)
        tmp = np.where(tmp >= .57, 1., tmp)
        tmp = tmp / .32 - .78125
    elif f_idx == 31:
        tmp = np.where(array <= .42, 0., array)
        tmp = np.where(tmp >= .92, 1., tmp)
        tmp = 2. * tmp - .84
    elif f_idx == 32:
        tmp = np.where(array <= .42, 4. * array, array)
        tmp = np.where(tmp <= .92, -2. * tmp + 1.84, tmp / .08 - 11.5)
    elif f_idx == 33:
        tmp = np.abs(2. * array - .5)
    elif f_idx == 34:
        tmp = 2. * array
    elif f_idx == 35:
        tmp = 2. * array - .5
    elif f_idx == 36:
        tmp = 2. * array - 1.

    tmp = np.where(tmp >= 1., 1., tmp)
    tmp = np.where(tmp <= 0., 0., tmp)
    return tmp


def colormap_gnuplot(val_1, val_2, val_3, size):
    tmp_sum = np.ones(size * 4, dtype=np.float32)
    tmp_sum[0::4] = _formula_func(val_1, np.linspace(.0, 1., size))
    tmp_sum[1::4] = _formula_func(val_2, np.linspace(.0, 1., size))
    tmp_sum[2::4] = _formula_func(val_3, np.linspace(.0, 1., size))

    return tmp_sum
